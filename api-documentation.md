
# BOOKIT API DOCUMENTATION

### How to use the api for bookit

#### See README.md for installation

## Endpoints

### GET

* /categories
*  /get-booking/bookingId
* /companies/categoryId
* /company-bookings/bookingCompanyId
* /company-bookings-view/bookingCompanyId

#### Needs api token (user must be logged in)
* /user/userId
* /company/bookingCompanyId
* /user-companies/userId
* /company-staff/bookingCompanyId
* /get-booked-appointments/bookingCompanyId
* /cancel-booking/userbookingId
* /user-booking/userbookingId
* /user-bookings
* /staff-times/userId/bookingCompanyId


#### GET - /categories
Gets all categories

#### GET - /get-booking/bookingId
Gets a specefic booking by bookingId

#### GET - /company-bookings/bookingCompanyId
Gets bookings created by a certain company

#### GET - /company-bookings/bookingCompanyId
Gets bookings created by a certain company, the bookings have upcoming or ongoing times connected to it

#### GET -  /company-bookings-view/bookingCompanyId
Gets bookings created by a certain company, the bookings do not have to have any upcoming or ongoing times connected to it

#### GET -  /user/userId
Gets a user by Id

##### Requirements
* Authorization - By logging in through the /login endpoint - use the api_token to send an Authorization Bearer.

#### GET -  /company/bookingCompanyId
Gets a company that is connected to the logged in user

##### Requirements
* Authorization - By logging in through the /login endpoint - use the api_token to send an Authorization Bearer.

#### GET -  /user-companies/userId
Gets all companies connected to the logged in user

##### Requirements
* Authorization - By logging in through the /login endpoint - use the api_token to send an Authorization Bearer.

#### GET -  /company-staff/bookingCompanyId
Gets all users connected to a company

##### Requirements
* Authorization - By logging in through the /login endpoint - use the api_token to send an Authorization Bearer.

#### GET -  /get-booked-appointments/bookingCompanyId
Gets all booked appointments for a company.

##### Requirements
* Authorization - By logging in through the /login endpoint - use the api_token to send an Authorization Bearer.

#### GET -  /cancel-booking/userbookingId
Cancel a reservation

##### Requirements
* Authorization - By logging in through the /login endpoint - use the api_token to send an Authorization Bearer.

#### GET -  /user-booking/userbookingId
Get a user reservation for the logged in user

##### Requirements
* Authorization - By logging in through the /login endpoint - use the api_token to send an Authorization Bearer.

#### GET -  /user-bookings
Get all user reservations for the logged in user

##### Requirements
* Authorization - By logging in through the /login endpoint - use the api_token to send an Authorization Bearer.

#### GET -  /staff-times/userId/bookingCompanyId
Get all booked times connected to a company and user

##### Requirements
* Authorization - By logging in through the /login endpoint - use the api_token to send an Authorization Bearer.

### POST
* /register
* /login
* /logout
* /add-category


#### Needs api token (user must be logged in)


* /add-company
* /add-booking-info
* /update-userinfo/userId
* /update-booking-company/bookingCompanyId
* /update-booking/bookingId
* /reserve-booking/bookingId
* /update-user-booking/userbookingId
*  /add-booking-times Date-format should be: *2018-09-01* Time format should be: *09:00:00*
* /add-company-user

#### POST - /register
Register a user.

##### Requirements
* f_name
* l_name
* email
* password
* password_confirmation
* mobile_number (optional)
* address (optional)
* town (optional)
* zip (optional)

#### POST - /login
User gets logged in.

##### Requirements
* email
* password

#### POST - /logout
User gets logged out.

#### POST - /add-category
Adds a category

##### Requirements
* name
* image (Name of an image in /images folder) *TODO: implement fileupload*

#### POST - /update-userinfo/userId
Update user info for the logged in user.

##### Requirements
* Authorization - By logging in through the /login endpoint - use the api_token to send an Authorization Bearer.
* Update all/some/none columns in users table

#### POST - /update-booking-company/bookingCompanyId
Update booking-company

##### Requirements
* Authorization - By logging in through the /login endpoint - use the api_token to send an Authorization Bearer.
*  User sending the request to this endpoint needs to have user_level *company*  - Change this directly in the database *TODO: admin should be able to do this*
* Update all/some/none columns in booking_companies table

#### POST - /add-company
Adds a company

##### Requirements
* Authorization - By logging in through the /login endpoint - use the api_token to send an Authorization Bearer.s
* category_id
* company_name
* email
* address
* town (optional)
* mobile_number (optional)
* image (optional) (Name of an image in /images folder) *TODO: implement fileupload*
* webpage (optional)
* description (optional)

#### POST - /add-booking-info
Adds booking-info

##### Requirements
* Authorization - By logging in through the /login endpoint - use the api_token to send an Authorization Bearer.
* user_id
* company_id
* title
* image (optional) (Name of an image in /images folder) *TODO: implement fileupload*
* price (optional)
* description (optional)

#### POST - /add-booking-times
Adds times connected to a user, company and booking

##### Requirements
* Authorization - By logging in through the /login endpoint - use the api_token to send an Authorization Bearer.

* user_id
* company_id
* booking_id
* capacity
* start_date
* end_date
* start_time
* end_time

#### POST - /add-company-user
Connects a user to a company and alters the user_level to *staff*.

##### Requirements
* Authorization - By logging in through the /login endpoint - use the api_token to send an Authorization Bearer.
* User sending the request to this endpoint needs to have user_level *company* or *admin* - Change this directly in the database *TODO: admin should be able to do this*

* user_id
* company_id
* booking_id
* capacity
* start_date
* end_date
* start_time
* end_time

#### POST - /reserve-booking/bookingId
User makes a booking reservation

##### Requirements
* Authorization - By logging in through the /login endpoint - use the api_token to send an Authorization Bearer.
* user_id
* booking_id
* f_name
* l_name
* email
* mobile_numer (optional)
* address (optional)
* town (optional)
* zip (optional)
* message (optional)

#### POST - /update-user-booking/userbookingId
User makes a booking reservation

##### Requirements
* Authorization - By logging in through the /login endpoint - use the api_token to send an Authorization Bearer.
* Update all/some/none columns in user_bookings table

----
