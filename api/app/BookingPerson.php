<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BookingPerson extends Model
{
    protected $fillable = ['booking_id', 'company_id', 'f_name', 'l_name', 'email', 'mobile_number'];
}
