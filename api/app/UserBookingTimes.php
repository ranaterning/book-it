<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserBookingTimes extends Model
{
    protected $fillable = ['user_booking_id', 'booking_times_id', 'booking_company_id'];
}
