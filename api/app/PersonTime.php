<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PersonTime extends Model
{
    protected $fillable = ['booking_person_id', 'booking_times_id'];
}
