<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\CompanyUser;

class CompanyUserController extends Controller
{
    public function store(Request $request)
    {   //Add user_id and company id to company_users table
        $user_level = User::where('id', auth()->user()->id)->get();

        if($user_level[0]->user_level == 'company' || $user_level[0]->user_level == 'admin') {

            $company_user = CompanyUser::create($request->all());

            if((int)$request->user_id !== auth()->user()->id) {
                User::where('id', $request->user_id)->update(['user_level' => 'staff']);
            }

            return response()->json(true, 201);

        } else {
            return response()->json(null, 404);
        }
    }
}
