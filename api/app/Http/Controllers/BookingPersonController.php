<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Booking;
use App\Category;
use App\BookingPerson;
use App\BookingCompany;
use App\BookingTime;
use App\Time;
use App\User;
use App\UserBookingTimes;
use App\UserBooking;

class BookingPersonController extends Controller
{

    //Show all times to a specefic company and selected user
    public function showBookedUserAppointments($user_id, $BookingCompany_id)
    {
        $user_booking_times = UserBookingTimes::where('booking_company_id', $BookingCompany_id)->get();

        $booked_appointments = [];
        $appointments = [];
        $count = 0;

        for ($i=0; $i < count($user_booking_times); $i++) {

            $booking_time = BookingTime::where([ ['id', $user_booking_times[$i]->booking_time_id], ['status', 'booked'], ['user_id', $user_id] ])->get();

            if(count($booking_time) > 0) {
                array_push($booked_appointments, UserBooking::where('id', $user_booking_times[$i]->user_booking_id)->get());
                $appointments[$count] = $booked_appointments[$count][0];
                $appointments[$count]['appointment'] = $booking_time;

                $count++;
            }

        }

        return response()->json($booked_appointments, 200);

    }

}
