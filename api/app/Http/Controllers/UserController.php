<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;


class UserController extends Controller
{
    public function show(User $user)
    {
        if($user->id == auth()->user()->id){
            return response()->json($user, 201);
        } else {
            return response()->json(null, 404);
        }
    }

    public function update(Request $request, User $user)
    {
    	if($user->id == auth()->user()->id){
        	$user->update($request->all());
        	return response()->json($user, 200);
    	}else {
        return response()->json(null, 400);
      }
    }
}
