<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Booking;
use App\BookingTime;
use App\BookingPerson;
use App\UserBooking;
use App\UserBookingTimes;
use App\Http\Controllers\UserBookingTimeController as UserBookingTimeController;

class UserBookingController extends Controller
{
    public function index()
    {
        $check_status = new UserBookingTimeController;
        $bookings = $check_status->getUserBookings();
        return $bookings;
    }

    public function show(UserBooking $userbooking)
    {
        $check_status = new UserBookingTimeController;
        $booking = $check_status->getUserBooking($userbooking);
        if(!empty($booking)){
            return response()->json($booking, 200);
        } else {
            return response()->json(null, 404);
        }
    }

    public function store(Request $request, Booking $booking)
    {
        if($request->user_id == auth()->user()->id) {

            $add_user_booking = new UserBookingTimeController;
            $reservation = $add_user_booking->checkBookingTimeCapacity($booking, $request);
            $booking['booking'] = $reservation;

            return response()->json($booking, 201);

        } else {
            return response()->json(null, 404);
        }
    }

    public function update(UserBooking $UserBooking, Request $request)
    {
        $UserBooking->update($request->all());
        return response()->json($UserBooking, 200);
    }

    public function cancel(UserBooking $userbooking)
    {
        if($userbooking->user_id == auth()->user()->id) {

            $user_booking_time = UserBookingTimes::where( [ ['user_booking_id', $userbooking->id], ['status', 'booked'] ])->get();
            UserBookingTimes::where( [ ['user_booking_id', $userbooking->id], ['status', 'booked'], ['booking_time_id', $user_booking_time[0]->booking_time_id] ])->update(['status' => 'canceled']);
            BookingTime::where([ ['id', $user_booking_time[0]->booking_time_id] ])->increment('capacity', 1);
            UserBookingTimes::where([ ['status', 'canceled'] ])->delete();
            UserBooking::where('id',  $userbooking->id)->delete();

            return response()->json('Canceled', 204);

        } else {
            return response()->json(null, 404);
        }
    }
}
