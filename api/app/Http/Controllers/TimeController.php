<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Booking;
use App\Category;
use App\BookingTime;
use App\BookingPerson;
use Carbon\Carbon;
use App\UserBooking;
use App\UserBookingTimes;
use App\BookingCompany;
use App\PersonTime;
use App\CompanyUser;
use App\User;

class TimeController extends Controller
{
	public function __construct()
	{
       $this->dt = Carbon::now();
       $this->currentDate = $this->dt->toDateString();
       $this->currentTime = $this->dt->toTimeString();
    }

	public function getBookings()
	{
		//Updating status of the times that have passed the current date/time
		BookingTime::where('start_date', '<', $this->currentDate)->orWhere([['start_date', '=', $this->currentDate], ['start_time', '<', $this->currentTime]])->update(['status' => 'completed']);

		//Get all bookings
		$booking_time = BookingTime::where('status', '!=', 'completed')->pluck('booking_id');

		if($booking_time->count() > 1) {
	 		$bookings = Booking::whereIn('id', $booking_time)->get();
			return $bookings;
		} else {
			return $bookings = Booking::all();
		}

	}


	public function getBooking($booking)
	{
		//Getting the booking
		$booking_company = BookingCompany::where('id', $booking->company_id)->get();
		$booking['company'] = $booking_company[0];

		//Updating status of the times that have passed the current date/time
		BookingTime::where('start_date', '<', $this->currentDate)->orWhere([['start_date', '=', $this->currentDate], ['start_time', '<', $this->currentTime]])->update(['status' => 'completed']);

		$user_ids = CompanyUser::where('company_id', $booking->company_id)->pluck('user_id');

		$booking_staff = [];
		$staff = [];

		$count = 0;
		for ($i=0; $i < count($user_ids); $i++) {

			array_push($booking_staff, User::where('id', $user_ids[$i])->get());

			if($booking_staff[$i][0]->id === $user_ids[$i]) {
				$staff[$count] = $booking_staff[$i][0];
				$staff[$count]['times'] = BookingTime::where([ ['user_id', $booking_staff[$i][0]->id], ['booking_id', $booking->id], ['status', '!=', 'completed']])->get();
				$count++;
			}
		}

		$booking['booking_staff'] = $staff;
		return $booking;

	}

}
