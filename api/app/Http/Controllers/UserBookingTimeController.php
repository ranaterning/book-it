<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Booking;
use App\Category;
use App\BookingTime;
use App\BookingPerson;
use Carbon\Carbon;
use App\UserBooking;
use App\UserBookingTimes;
use App\PersonTime;
use App\BookingCompany;
use App\User;

class UserBookingTimeController extends Controller
{
	public function __construct()
	{
       $this->dt = Carbon::now();
       $this->currentDate = $this->dt->toDateString();
       $this->currentTime = $this->dt->toTimeString();
    }

	public function getUserBookings()
	{
		//Updating status of the times that have passed the current date/time
		BookingTime::where('start_date', '<', $this->currentDate)->orWhere([['start_date', '=', $this->currentDate], ['start_time', '<', $this->currentTime]])->update(['status' => 'completed']);
		//Get all user bookings
		$completed_user_booking_time_ids = BookingTime::where('status', '=', 'completed')->pluck('id');
		UserBookingTimes::whereIn('booking_time_id', $completed_user_booking_time_ids)->update(['status' => 'completed']);

		$user_booking_ids = UserBookingTimes::where('status', '!=', 'completed')->pluck('user_booking_id');
		$user_bookings = Booking::whereIn('user_bookings.id', $user_booking_ids)->where('user_bookings.user_id', auth()->user()->id)->join('user_bookings', 'bookings.id', '=', 'user_bookings.booking_id')->get();

		return $user_bookings;
	}

	public function getUserBooking($user_booking)
	{
		//Getting the user-booking
		$check_user_booking_id = UserBooking::where('id', $user_booking->id)->get();

		if($check_user_booking_id[0]->id >= 1){

			//Updating status of the times that have passed the current date/time
			BookingTime::where('start_date', '<', $this->currentDate)->orWhere([['start_date', '=', $this->currentDate], ['start_time', '<', $this->currentTime]])->update(['status' => 'completed']);

			$user_booking_time = UserBookingTimes::where([ ['user_booking_id', $user_booking->id],['status', 'booked'] ])->get();

			if(count($user_booking_time) > 0) {

				$time = BookingTime::where([['id', '=', $user_booking_time[0]->booking_time_id], ['status', '!=', 'completed']])->get();
				if(count($time) > 0){
					$booking = Booking::where('id', $check_user_booking_id[0]->booking_id)->get();
					$booking_staff = User::where('id', $time[0]->user_id)->get();
					$company = BookingCompany::where('id', $booking[0]->company_id)->get();
					$booking['company'] = $company;
					$booking['booking_staff'] = $booking_staff;
					$booking['booking_staff']['time'] = $time;
					$booking['user_booking'] = $user_booking;

					return $booking;
				}
			}
		} else {
			return response()->json(null, 404);
		}

	}

	public function checkBookingTimeCapacity($booking,$request)
	{
		$check_booking_times = BookingTime::whereIn('id', $request->time)->get();

		foreach ($check_booking_times as $key => $times) {

		    if(!$times->capacity < 1) {

		        BookingTime::where('id', $times->id)->decrement('capacity', 1);
		        BookingTime::where('id', $times->id)->update(['status' => 'booked']);

		    	self::addUserBooking($booking,$request);

		    } else {
		       return false;
		    }
		}
	}

	public function addUserBooking($booking, $request)
	{
        $request->request->add(['booking_id' => $booking->id]);
        $user_booking = UserBooking::create($request->all());

        $user_booking_times = [];
        foreach ($request->time as $key => $time) {
            $user_booking_times = ['user_booking_id' => $user_booking->id, 'booking_time_id' => $time['booking_time_id'], 'booking_company_id' => $booking->company_id];
            UserBookingTimes::insert($user_booking_times);
        }

		$user_reservation = self::getUserBooking($user_booking);
		$booking['reservation'] = $user_reservation;
		return $booking;
	}

}
