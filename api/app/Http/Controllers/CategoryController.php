<?php

namespace App\Http\Controllers;

use App\Http\Controllers\TimeController as TimeController;
use Illuminate\Http\Request;
use App\Booking;
use App\Category;
use App\BookingTime;
use App\BookingPerson;
use Carbon\Carbon;

class CategoryController extends Controller
{
    public function index()
    {   //Returns all categories
        return Category::all();
    }

    public function show(Booking $booking)
    {   //Returns a certain booking
        $check_status = new TimeController;
        $booking = $check_status->getBooking($booking);

        return response()->json($booking, 201);
    }

    public function store(Request $request)
    {   //Stores a category
        $category = Category::create($request->all());
        return response()->json($category, 201);
    }

    public function update(Request $request, Category $category)
    {   //Updates a category
        $category->update($request->all());
        return response()->json($category, 200);
    }

    public function delete(Request $request, Category $category)
    {   //Deletes a category
        $category->delete();
        return response()->json(null, 204);
    }
}
