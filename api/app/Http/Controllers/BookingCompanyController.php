<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\BookingCompany;
use App\CompanyUser;
use App\Category;
use App\Booking;
use App\User;
use App\Time;

class BookingCompanyController extends Controller
{
    public function index(BookingCompany $BookingCompany)
    {   //Returns all companies
        $BookingCompany->bookings = Booking::where('company_id', $BookingCompany->id)->get();
        return response()->json($BookingCompany, 201);
    }

    public function show(BookingCompany $BookingCompany)
    {   //Returns bookings created by a certain company
        $check_status = new TimeController;
        $BookingCompany->bookings = $check_status->getBookings()->where('company_id', $BookingCompany->id);

        return response()->json($BookingCompany, 201);
    }

    public function showUserCompany(BookingCompany $BookingCompany){
        return response()->json($BookingCompany, 201);
    }

    public function showUserCompanies(User $user)
    {
        //Returns all companies connected to a user
        $comp_id = CompanyUser::where('user_id', $user->id)->pluck('company_id');

        $comps = BookingCompany::whereIn('id', $comp_id)->get();
        $user['company'] = $comps;

        return response()->json($user['company'], 201);
    }

    public function showAll(Category $category)
    {
        //Show all companies in a certain category
        $companies = BookingCompany::where('category_id', $category->id)->get();
        return response()->json($companies, 201);
    }

    public function store(Request $request)
    {
        //TODO only user_level admin should be able to add companies!

        //Add a company to the database if user_level is company
        $user_level = User::where('id', auth()->user()->id)->get();

        if($user_level[0]->user_level == 'company' || $user_level[0]->user_level == 'admin') {

            $bookingcompanies = BookingCompany::create($request->all());

            $company_users = new CompanyUser;
            $company_users->user_id = auth()->user()->id;
            $company_users->company_id = $bookingcompanies->id;
            $company_users->save();

            return response()->json($bookingcompanies, 201);
        } else {
            return response()->json(null, 404);
        }

    }

    public function showCompanyStaff(BookingCompany $BookingCompany)
    {
        $user_ids = CompanyUser::where('company_id', $BookingCompany->id)->pluck('user_id');

        $booking_staff = [];
        $staff = [];

        $count = 0;
        for ($i=0; $i < count($user_ids); $i++) {

            array_push($booking_staff, User::where('id', $user_ids[$i])->get());

            if($booking_staff[$i][0]->id === $user_ids[$i]) {
                $staff[$count] = $booking_staff[$i][0];
                $count++;
            }
        }

        $BookingCompany['company_staff'] = $staff;
        return response()->json($BookingCompany, 201);
    }

    public function update(Request $request, BookingCompany $BookingCompany)
    {
        $BookingCompany->update($request->all());
        return response()->json($BookingCompany, 200);
    }

    public function delete(Request $request, BookingCompany $bookingcompanies)
    {   //Delete company-info
        if($bookingcompanies->user_id === auth()->user()->id){
            $bookingcompanies->delete();
            return response()->json(null, 204);
        } else {
            return response()->json(null, 404);
        }
    }
}
