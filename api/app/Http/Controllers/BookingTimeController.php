<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Controllers\TimeController as TimeController;

use App\Booking;
use App\BookingTime;
use App\BookingPerson;
use App\PersonTime;
use Carbon\Carbon;

class BookingTimeController extends Controller
{

    // TODO a user should not be able to be connected to the same time and date more than once!!!!
    public function store(Request $request)
    {
        //Adding time and dates for a booking and bookingperson to the database
        $times = [];
        $request->time = $request->request->get('time');

        foreach ($request->time as $key => $time) {
            BookingTime::insert($time);
            array_push($times, $time);
        }

        return response()->json($times, 201);
    }

}
