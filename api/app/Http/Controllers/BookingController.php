<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Controllers\TimeController as TimeController;

use App\Booking;
use App\BookingTime;
use App\BookingPerson;
use App\PersonTime;
use Carbon\Carbon;
use App\UserBooking;
use App\BookingCompany;
use App\UserBookingTimes;
use App\User;

class BookingController extends Controller
{

    public function index()
    {   //Returns all bookings created by the loggedin user
        $check_status = new TimeController;
        $bookings = $check_status->getBookings()->where('user_id', auth()->user()->id);

        return response()->json($bookings, 200);
    }

    public function show(Booking $booking)

    {   //Returns a booking created by the loggedin user
        if($booking->user_id === auth()->user()->id) {

            $check_status = new TimeController;
            $booking = $check_status->getBooking($booking);

            return response()->json($booking, 200);

        } else {
            return response()->json(null, 404);
        }
    }

    public function showBookedAppointments(BookingCompany $BookingCompany)
    {
        $user_booking_times = UserBookingTimes::where('booking_company_id', $BookingCompany->id)->get();

        $booked_appointments = [];
        $appointments = [];
        $count = 0;

        for ($i=0; $i < count($user_booking_times); $i++) {

            $booking_time = BookingTime::where([ ['id', $user_booking_times[$i]->booking_time_id], ['status', 'booked'] ])->get();

            if(count($booking_time) > 0) {
                array_push($booked_appointments, UserBooking::where('id', $user_booking_times[$i]->user_booking_id)->get());
                $appointments[$count] = $booked_appointments[$count][0];
                $appointments[$count]['appointment'] = $booking_time;

                $count++;
            }

        }

        return response()->json($booked_appointments, 200);
    }

    public function store(Request $request)
    {
        //Adding the booking info to the database
        $user_level = User::where('id', auth()->user()->id)->get();

        if($user_level[0]->user_level == 'company' || $user_level[0]->user_level == 'admin') {
            $booking = Booking::create($request->all());
            return response()->json($booking, 201);
        } else {
            return response()->json(null, 400);
        }
    }

    public function connect(Request $request)
    {
        $person_times = PersonTime::insert($request->person_times);
        return response()->json($person_times, 201);
    }

    public function update(Request $request, Booking $booking)
    {   //Update a specific booking
        if($booking->user_id === auth()->user()->id){

            $booking->update($request->all());
            return response()->json($booking, 201);

        } else {
            return response()->json(null, 404);
        }
    }

    public function delete(Request $request, Booking $booking)
    {   //Delete a booking
        if($booking->user_id === auth()->user()->id){
            $booking->delete();
            return response()->json(null, 204);
        } else {
            return response()->json(null, 404);
        }
    }
}
