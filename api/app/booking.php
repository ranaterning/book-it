<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
     protected $fillable = [
    	'user_id',
        'company_id',
    	'title',
        'price',
    	'description',
        'image',
    ];
}
