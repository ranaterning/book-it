<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BookingTime extends Model
{
    protected $fillable = ['booking_id', 'start_date', 'end_date', 'start_time', 'end_time'];
}
