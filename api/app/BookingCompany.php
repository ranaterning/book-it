<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BookingCompany extends Model
{
    protected $fillable = ['category_id', 'company_name', 'description', 'mobile_number', 'address', 'town', 'email', 'webpage', 'image'];
}
