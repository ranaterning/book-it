<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserBooking extends Model
{
    protected $fillable = [
    'user_id', 
    'booking_id', 
    'f_name',
    'l_name',
    'email',
    'mobile_number',
    'address',
    'town',
    'city',
    'zip',
    'message'
    ];
}
