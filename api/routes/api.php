<?php
use Illuminate\Http\Request;

//API Routes
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

	Route::post('register', 'Auth\RegisterController@register'); //Register a user
	Route::post('login', 'Auth\LoginController@login'); //Log in a user
	Route::post('logout', 'Auth\LoginController@logout'); //Log out a user

	//CategoryController
	Route::get('categories/', 'CategoryController@index'); //Show all categories
	Route::get('get-booking/{booking}', 'CategoryController@show'); //Show a booking by bookingId
	Route::post('add-category/', 'CategoryController@store'); //Add a new category TODO admin should be able to do this
	Route::delete('delete-category/{category}', 'CategoryController@delete'); //Delete a category TODO admin should be able to do this
    Route::post('update-category/{category}', 'CategoryController@update'); //Update a category TODO admin should be able to do this

    //BookingCompanyController
	Route::get('companies/{category}', 'BookingCompanyController@showAll'); //Show all companies belonging to a category
	Route::get('company-bookings/{BookingCompany}', 'BookingCompanyController@show');//Show all bookings created by a company
    Route::get('company-bookings-view/{BookingCompany}', 'BookingCompanyController@index'); //Show all bookings created by a company that has no times connected to it

Route::group(['middleware' => 'auth:api'], function() {
	Route::get('user/{user}', 'UserController@show'); //Get a user
    Route::post('update-userinfo/{user}', 'UserController@update'); //Update a users info

	//BookingCompanyController
    Route::get('company/{BookingCompany}', 'BookingCompanyController@showUserCompany'); //Show a certain company
	Route::get('user-companies/{user}', 'BookingCompanyController@showUserCompanies'); //Show companies connected to a specefic user
	Route::post('update-booking-company/{BookingCompany}', 'BookingCompanyController@update'); //Update a company
	Route::post('add-company/', 'BookingCompanyController@store'); //Add a company TODO only admin should be able to do this
	Route::delete('delete-bookingcompany/{BookingCompany}', 'BookingCompanyController@delete'); //Delete a booking belonging to the user
    Route::get('company-staff/{BookingCompany}','BookingCompanyController@showCompanyStaff'); //Show all people connected to a specefic company

	//BookingController
	Route::post('add-booking-info/', 'BookingController@store'); //Add booking-info
    Route::get('get-booked-appointments/{BookingCompany}', 'BookingController@showBookedAppointments'); //Show booked appointments for a certain companies bookings
	Route::post('update-booking/{booking}', 'BookingController@update'); //Update an existing booking belonging to the user
	Route::delete('delete-booking/{booking}', 'BookingController@delete'); //Delete a booking belonging to the user


	//UserBookingController
	Route::post('reserve-booking/{booking}','UserBookingController@store'); //Make an appointment
	Route::get('cancel-booking/{userbooking}','UserBookingController@cancel'); //Cancel an appointment
	Route::get('user-booking/{userbooking}','UserBookingController@show'); //Show a reservation for the logged in user by userbookingId
	Route::get('user-bookings','UserBookingController@index'); //shows all reservations for the logged in user
    Route::post('update-user-booking/{UserBooking}','UserBookingController@update'); //Update a users reservation

    //BookingPersonController
    Route::get('staff-times/{User}/{BookingCompany}','BookingPersonController@showBookedUserAppointments'); //Show all booking-times connected to a specific user and company

    //BookingTimeController
    Route::post('add-booking-times','BookingTimeController@store');
    //TODO delete a date/time
    //TODO Update a date/time

    //CompanyUserController
    Route::post('add-company-user','CompanyUserController@store');
    //TODO delete company user
});
