<?php
//
// namespace Tests\Feature;
//
// use Tests\TestCase;
// use Illuminate\Foundation\Testing\WithFaker;
// use Illuminate\Foundation\Testing\RefreshDatabase;
//
// class RegisterTest extends TestCase
// {
//     public function testsRegistersSuccessfully()
//     {
//         $payload = [
//             'f_name' => 'John',
//             'l_name' => 'Johnsson',
//             'email' => 'john@toptal.com',
//             'password' => 'toptal123',
//             'password_confirmation' => 'toptal123',
//             'mobile_number' => '0737053638',
//             'address' => 'Bjulevägen 26 C',
//             'town' => 'Stockholm',
//             'zip' => '122 41'
//         ];
//
//         $this->json('post', '/api/register', $payload)
//             ->assertStatus(201)
//             ->assertJsonStructure([
//                 'data' => [
//                     'id',
//                     'f_name',
//                     'l_name',
//                     'email',
//                     'mobile_number',
//                     'address',
//                     'town',
//                     'zip',
//                     'created_at',
//                     'updated_at',
//                     'api_token',
//                 ],
//             ]);;
//     }
//
//     public function testsRequiresPasswordEmailAndName()
//     {
//         $this->json('post', '/api/register')
//             ->assertStatus(422)
//             ->assertJson([
//                 'f_name' => ['The first name field is required.'],
//                 'l_name' => ['The last name field is required.'],
//                 'email' => ['The email field is required.'],
//                 'password' => ['The password field is required.'],
//             ]);
//     }
//
//     public function testsRequirePasswordConfirmation()
//     {
//         $payload = [
//             'f_name' => 'John',
//             'l_name' => 'Johnsson',
//             'email' => 'john@toptal.com',
//             'password' => 'toptal123',
//         ];
//
//         $this->json('post', '/api/register', $payload)
//             ->assertStatus(422)
//             ->assertJson([
//                 'password' => ['The password confirmation does not match.'],
//             ]);
//     }
// }
