<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UserBookingTime extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_booking_times', function (Blueprint $table) {
            $table->integer('user_booking_id')->unsigned();
            $table->foreign('user_booking_id')->references('id')->on('user_bookings')->onDelete('cascade');
            $table->integer('booking_time_id')->unsigned();
            $table->foreign('booking_time_id')->references('id')->on('booking_times')->onDelete('cascade');
            $table->integer('booking_company_id')->unsigned();
            $table->foreign('booking_company_id')->references('id')->on('booking_companies')->onDelete('cascade');
            $table->enum('status', array('booked', 'canceled', 'completed'))->default('booked');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_booking_times');
    }
}
