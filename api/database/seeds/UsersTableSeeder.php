<?php

use Illuminate\Database\Seeder;
use App\User;
class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

      $faker = \Faker\Factory::create();

      // Let's make sure everyone has the same password and
      // let's hash it before the loop, or else our seeder
      // will be too slow.
      $password = Hash::make('toptal');

      // And now let's generate a few dozen users for our app:
      for ($i = 0; $i < 5; $i++) {
          User::create([
              'f_name' => $faker->name,
              'l_name' => $faker->name,
              'email' => $faker->email,
              'password' => $password,
              'mobile_number' => '0737053638',
              'address' => 'Bjulevägen 26 C',
              'town' => 'Stockholm',
              'zip' => '122 41'
          ]);
      }
    }
}
