
//get all booked appointments for a certain companies bookings
function getBookedAppointments() {

    $(".user-icons").find("div").css("background-color", "#f7f6f6");
    $(".icon-clock").css("background-color", "#e6d5d5");

    document.getElementById("page-title").innerHTML = "BOKADE TIDER";

    closeMenu();
    container.innerHTML = "";

    var settings = {
      "async": true,
      "crossDomain": true,
      "url": "http://localhost/~ranaterning/htdocs/book-it/api/public/api/user-companies/"+userId,
      "method": "GET",
      "headers": {
        "Authorization": "Bearer "+apiToken
      }
    }

    $.ajax(settings).done(function (response) {
        var schedule = document.createElement("a");
        schedule.innerHTML = "Mitt schema";
        schedule.className = "page-link";
        container.appendChild(schedule);
        schedule.addEventListener('click', getPersonalSchedule);

        var x;
        var selectArray = [];

        for(x in response){
            selectArray.push(response[x].company_name);
        }

        var selectList = document.createElement("select");
        selectList.id = "select";

        for (var i = 0; i < selectArray.length; i++) {
          var option = document.createElement("option");
          option.value = selectArray[i];
          option.text = selectArray[i];
          option.id = response[i].id;
          selectList.appendChild(option);
        }

        container.appendChild(selectList);

        var calendarContainer = document.createElement("div");
        calendarContainer.id = "calendarContainer";
        container.appendChild(calendarContainer);

            function appointments() {

                $("#calendarContainer").html("");

                var selectedCompanyId = $("#select").find('option:selected').attr('id');

                var settings = {
                  "async": true,
                  "crossDomain": true,
                  "url": "http://localhost/~ranaterning/htdocs/book-it/api/public/api/get-booked-appointments/"+selectedCompanyId,
                  "method": "GET",
                  "headers": {
                    "Authorization": "Bearer "+apiToken,
                  }
                }

                $.ajax(settings).done(function (response) {
                    if(response.length >= 1) {

                      var timeData = [];
                      var j = 0;

                      for (var i = 0; i < response.length; i++) {
                          timeData[j] = {};
                          timeData[j]["id"] = response[i][0].id;
                          timeData[j]["title"] = response[i][0].appointment[0].start_time+"-"+response[i][0].appointment[0].end_time;
                          timeData[j]["start"] = response[i][0].appointment[0].start_date+"T"+response[i][0].appointment[0].start_time;
                          timeData[j]["end"] = response[i][0].appointment[0].end_date+"T"+response[i][0].appointment[0].end_time;
                          j++;
                      }

                      var calendarDiv = document.createElement("div");
                      calendarDiv.id = "calendar";
                      calendarContainer.appendChild(calendarDiv);

                      document.getElementById("calendar").innerHTML = "";

                      var months = ["01","02","03","04","05","06","07","08","09","10","11","12"];
                      var date = new Date();
                      var year = date.getFullYear();
                      var monthIndex = date.getMonth();
                      var day = date.getDate();

                      var fullDate = year + "-" + months[monthIndex] + "-" + day;

                      $(document).ready(function() {
                        var initialLocaleCode = 'sv';

                        $('#calendar').fullCalendar({
                          header: {
                            left: 'prev,next today',
                            center: 'title',
                            right: 'month,agendaWeek,agendaDay,listMonth'
                          },
                          defaultDate: fullDate,
                          locale: initialLocaleCode,
                          buttonIcons: false,
                          weekNumbers: true,
                          navLinks: true,
                          editable: true,
                          eventLimit: true,
                          events: timeData,
                          eventClick: function(event) {
                             if (event) {

                                 var settings = {
                                      "async": true,
                                      "crossDomain": true,
                                      "url": "http://localhost/~ranaterning/htdocs/book-it/api/public/api/user-booking/"+event.id,
                                      "method": "GET",
                                      "headers": {
                                        "Authorization": "Bearer "+apiToken
                                      }
                                    }

                                    $.ajax(settings).done(function (response) {
                                        container.innerHTML = "";

                                         document.getElementById("page-title").innerHTML = response[0].title;

                                         var userBookingDiv = document.createElement("div");
                                         userBookingDiv.className = "thanks-page";
                                         userBookingDiv.id = "thanks-page";
                                         container.appendChild(userBookingDiv);

                                         var bookingInfoDiv = document.createElement("div");
                                         bookingInfoDiv.className = "booking-info";
                                         bookingInfoDiv.id = "booking-info";
                                         userBookingDiv.appendChild(bookingInfoDiv);

                                         var ul = document.createElement("ul");
                                         ul.id = 'info';
                                         bookingInfoDiv.appendChild(ul);

                                         var info = [response[0].title];
                                         var titles = ['Tjänst:', 'Tid:', 'Datum', 'Kund'];

                                         time = response.booking_staff.time[0].start_time+"-"+response.booking_staff.time[0].end_time;
                                         name = response.user_booking.f_name+" "+response.user_booking.l_name;
                                         info.push(time, response.booking_staff.time[0].start_date, name);

                                         var text = "";
                                         for (var i = 0; i < info.length; i++ ) {
                                         		text += "<li> <strong>"+titles[i]+"</strong> <div><p>"+info[i]+"</p></div></li>";
                                         }

                                         document.getElementById("info").innerHTML = text;

                                    });
                             }
                           }
                      });
                    });
                } else {
                    var emptyH1 = document.createElement("h1");
                    emptyH1.innerHTML = "INGA BOKADE TIDER ÄN";
                    container.appendChild(emptyH1);
                }
            });
            }

        appointments();
        $( "#select" ).change(appointments);

    });
}
