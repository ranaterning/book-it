var userId = readCookie('user_id');
var apiToken = readCookie('api_token');
var address;

//User page
function userPage(e) {
	e.preventDefault();

	closeMenu();
	container.innerHTML = "";
	document.getElementById("user-menu").innerHTML = "";
	document.getElementById("page-title").innerHTML = "";

	var settings = {
		  "async": true,
		  "crossDomain": true,
		  "url": "http://localhost/~ranaterning/htdocs/book-it/api/public/api/user/"+userId,
		  "method": "GET",
		  "headers": {
		    "Authorization": "Bearer "+apiToken
		  }
		}

		$.ajax(settings).done(function (response) {

			document.getElementById("page-title").innerHTML = response.f_name+ " " +response.l_name;
			var userDiv = document.createElement("div");
			userDiv.className = "user-page";
			document.getElementById("user-menu").appendChild(userDiv);

			var userIcons = document.createElement("div");
			userIcons.className = "user-icons";
			userDiv.appendChild(userIcons);

			//POFIL
			var cogIconDiv = document.createElement("div");
			cogIconDiv.className = "icon-cog";
			userIcons.appendChild(cogIconDiv);

			var cogP = document.createElement("p");
			var cogPText = document.createTextNode("PROFIL");
			cogP.appendChild(cogPText);
			cogIconDiv.appendChild(cogP);
			cogIconDiv.addEventListener('click', userPage);

			//MINA BOKNINGAR
			var clockIconDiv = document.createElement("div");
			userIcons.appendChild(clockIconDiv);
			clockIconDiv.className = "icon-clock";
			clockIconDiv.addEventListener('click', getUserReservations);

			var clockP = document.createElement("p");
			var clockPText = document.createTextNode("MINA BOKADE TIDER");
			clockP.appendChild(clockPText);
			clockIconDiv.appendChild(clockP);

			$(".user-icons").find("div").css("background-color", "#f7f6f6");
			$(".icon-cog").css("background-color", "#e6d5d5");

			var settings = {
				"async": true,
				"crossDomain": true,
				"url": "http://localhost/~ranaterning/htdocs/book-it/api/public/api/user/"+userId,
				"method": "GET",
				"headers": {
				"Authorization": "Bearer "+apiToken
				}
			}

			$.ajax(settings).done(function (response) {

				var userInfo = document.createElement("div");
				userInfo.className = "user_info";
				container.appendChild(userInfo);

				var editPenIcon = document.createElement("a");
				editPenIcon.className = "icon-pencil";
				var iconText = document.createTextNode("REDIGERA");
				editPenIcon.appendChild(iconText);
				userInfo.appendChild(editPenIcon);

				delete response.api_token;
				delete response.created_at;
				delete response.updated_at;
				delete response.user_level;

				var li = "";
				var i = 0;
				var userKeys = ["Användar id", "Förnamn", "Efternamn", "E-post", "Mobilnummer", "Address", "Stad", "Postkod"];
				for (var x in response) {
						li += "<li>" + userKeys[i] +": " +response[x] + "</li>";
					i++;
				}

				var ul = document.createElement("ul");
				userInfo.appendChild(ul);
				ul.innerHTML = li;

				editPenIcon.addEventListener("click", editUserInfoForm);

			});

			//edit user info form
			function editUserInfoForm(){
			    container.innerHTML = "";
			    document.getElementById("page-title").innerHTML = "";

			    document.getElementById("page-title").innerHTML = "ANVÄNDARUPPGIFTER";

			    $(".user-icons").find("div").css("background-color", "#f7f6f6");
			    $(".icon-cog").css("background-color", "#e6d5d5");

			    //Form for the userinfo
			    var f = document.createElement("form");
			    f.setAttribute('method',"post");

			    var fnameLabel = document.createElement("Label");
			    fnameLabel.htmlFor = "f_name";
			    fnameLabel.innerHTML="Förnamn:";

			    var fname = document.createElement("input");
			    fname.setAttribute('type',"text");
			    fname.setAttribute('name',"f_name");
			    fname.id = "f_name";
			    f.appendChild(fnameLabel);
			    f.appendChild(fname);

			    var lnameLabel = document.createElement("Label");
			    lnameLabel.htmlFor = "l_name";
			    lnameLabel.innerHTML="Efternamn:";

			    var lname = document.createElement("input");
			    lname.setAttribute('type',"text");
			    lname.setAttribute('name',"l_name");
			    lname.id = "l_name";
			    f.appendChild(lnameLabel);
			    f.appendChild(lname);

			    var emailLabel = document.createElement("Label");
			    emailLabel.htmlFor = "email";
			    emailLabel.innerHTML="E-post:";

			    var email = document.createElement("input");
			    email.setAttribute('type',"text");
			    email.setAttribute('name',"email");
			    email.id = "email";
			    f.appendChild(emailLabel);
			    f.appendChild(email);

			    var numberLabel = document.createElement("Label");
			    numberLabel.htmlFor = "number";
			    numberLabel.innerHTML="Mobil nummer:";

			    var number = document.createElement("input");
			    number.setAttribute('type',"text");
			    number.setAttribute('name',"mobile_number");
			    number.id = "mobile_number";
			    f.appendChild(numberLabel);
			    f.appendChild(number);

			    var townLabel = document.createElement("Label");
			    townLabel.htmlFor = "town";
			    townLabel.innerHTML="Stad:";

			    var town = document.createElement("input");
			    town.setAttribute('type',"text");
			    town.setAttribute('name',"town");
			    town.id = "town";
			    f.appendChild(townLabel);
			    f.appendChild(town);

			    var addressLabel = document.createElement("Label");
			    addressLabel.htmlFor = "address";
			    addressLabel.innerHTML="Address:";

			    var address = document.createElement("input");
			    address.setAttribute('type',"text");
			    address.setAttribute('name',"address");
			    address.id = "address";
			    f.appendChild(addressLabel);
			    f.appendChild(address);

			    var zipLabel = document.createElement("Label");
			    zipLabel.htmlFor = "zip";
			    zipLabel.innerHTML="Postnummer:";

			    var zip = document.createElement("input");
			    zip.setAttribute('type',"text");
			    zip.setAttribute('name',"zip");
			    zip.id = "zip";
			    f.appendChild(zipLabel);
			    f.appendChild(zip);

			    var btn = document.createElement("button");
			    btn.id = "booking-button";
			    btnText = document.createTextNode("REDIGERA");
			    btn.appendChild(btnText);
			    btn.addEventListener('click', editUserInfo);
			    f.appendChild(btn);

		        fname.value = response.f_name;
		        lname.value = response.l_name;
		        email.value = response.email;
		        number.value = response.mobile_number;
		        address.value = response.address;
		        town.value = response.town;
		        zip.value = response.zip;

			    container.appendChild(f);
		}

		});
}
