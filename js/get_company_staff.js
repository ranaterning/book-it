//get staff for a certain company
function getCompanyStaff() {
    container.innerHTML = "";

    var settings = {
      "async": true,
      "crossDomain": true,
      "url": "http://localhost/~ranaterning/htdocs/book-it/api/public/api/user-companies/"+userId,
      "method": "GET",
      "headers": {
        "Authorization": "Bearer "+apiToken
      }
    }

    $.ajax(settings).done(function (response) {

        var x;
        var selectArray = [];

        for(x in response){
            selectArray.push(response[x].company_name);
        }

        var selectList = document.createElement("select");
        selectList.id = "select";

        for (var i = 0; i < selectArray.length; i++) {
          var option = document.createElement("option");
          option.value = selectArray[i];
          option.text = selectArray[i];
          option.id = response[i].id;
          selectList.appendChild(option);
        }

        container.appendChild(selectList);

        var addStaffLink = document.createElement("a");
        addStaffLink .innerHTML = "Lägg till personal +";
        addStaffLink.className = "page-link";
        container.appendChild(addStaffLink);
        addStaffLink.addEventListener('click', function(){registerForm(false)});

        function getStaff() {
            $(".user-icons").find("div").css("background-color", "#f7f6f6");
            $("#user-menu icon-user").css("background-color", "#e6d5d5");

            selectedCompanyId =  $("#select").find('option:selected').attr('id');
            var settings = {
                "async": true,
                "crossDomain": true,
                "url": "http://localhost/~ranaterning/htdocs/book-it/api/public/api/company-staff/"+selectedCompanyId,
                "method": "GET",
                "headers": {
                    "Authorization": "Bearer "+apiToken,
                }
            }

            $.ajax(settings).done(function (response) {
                var ul = document.createElement("ul");

                for (var x in response.company_staff) {

                    var li = document.createElement("li");
                    li.className = "person-list";
                    var litext = document.createTextNode(response.company_staff[x].f_name + " " + response.company_staff[x].l_name);
                    li.appendChild(litext);
                    container.appendChild(li);
                }

            });
        }

        getStaff();
        $( "#select" ).change(getStaff);
    });
}
