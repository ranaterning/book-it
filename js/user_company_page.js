//User company page (Menu)
function userCompanyPage() {
    closeMenu();
    container.innerHTML = "";
    document.getElementById("user-menu").innerHTML = "";
    document.getElementById("page-title").innerHTML = "";

    var userDiv = document.createElement("div");
    userDiv.className = "user-page";
    document.getElementById("user-menu").appendChild(userDiv);

    var userIcons = document.createElement("div");
    userIcons.className = "user-icons";
    userDiv.appendChild(userIcons);

    var comIconDiv = document.createElement("div");
    comIconDiv.className = "icon-heart-empty";
    userIcons.appendChild(comIconDiv);

    if(readCookie('companies') < 1) {
        comIconDiv.addEventListener('click', function() {registerCompanyForm(true, null)} );
        registerCompanyForm(true, null);
    }else {
        userCompany();
        comIconDiv.addEventListener('click', userCompany);
    }

    var comP = document.createElement("p");
    var comPText = document.createTextNode("FÖRETAG/BOKNINGSÄGARE");
    comP.appendChild(comPText);
    comIconDiv.appendChild(comP);

    var calIconDiv = document.createElement("div");
    calIconDiv.className = "icon-calendar-empty";
    userIcons.appendChild(calIconDiv);
    calIconDiv.addEventListener('click', function() {getCompanyBookings(id = null,false) });

    var calP = document.createElement("p");
    var calPText = document.createTextNode("BOKNINGSBARA TILLFÄLLEN");
    calP.appendChild(calPText);
    calIconDiv.appendChild(calP);

    var clockIconDiv = document.createElement("div");
    userIcons.appendChild(clockIconDiv);
    clockIconDiv.className = "icon-clock";
    clockIconDiv.addEventListener('click', getBookedAppointments);

    var clockP = document.createElement("p");
    var clockPText = document.createTextNode("BOKADE TIDER");
    clockP.appendChild(clockPText);
    clockIconDiv.appendChild(clockP);

    var userIconDiv = document.createElement("div");
    userIcons.appendChild(userIconDiv);
    userIconDiv.className = "icon-user";
    userIconDiv.addEventListener('click', getCompanyStaff);

    var userP = document.createElement("p");
    var userPText = document.createTextNode("PERSONAL");
    userP.appendChild(userPText);
    userIconDiv.appendChild(userP);

}
