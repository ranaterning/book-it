window.addEventListener("load", function(e) {
    if(userId){
        checkUserCompanies();
    }
});

var userId = readCookie('user_id');
var apiToken = readCookie('api_token');
var imgSrc;

//Check if a user has a company connected to it
function  checkUserCompanies() {
    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "http://localhost/~ranaterning/htdocs/book-it/api/public/api/user-companies/"+userId,
        "method": "GET",
        "headers": {
            "Authorization": "Bearer "+apiToken
        }
    }

    $.ajax(settings).done(function (response) {
        if(response.length){
            createCookie('companies',response.length,7);
        }
    });
}
//Get company belonging to user
function userCompany() {
    closeMenu();
    container.innerHTML = "";

    $(".user-icons").find("div").css("background-color", "#f7f6f6");
    $(".icon-heart-empty").css("background-color", "#e6d5d5");

    var addCompanyLink = document.createElement("a");
    addCompanyLink .innerHTML = "Lägg till företag +";
    addCompanyLink.className = "page-link";
    container.appendChild(addCompanyLink);
    addCompanyLink.addEventListener('click', function() {registerCompanyForm(true, null)});

    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "http://localhost/~ranaterning/htdocs/book-it/api/public/api/user-companies/"+userId,
        "method": "GET",
        "headers": {
            "Authorization": "Bearer "+apiToken
        }
    }

    $.ajax(settings).done(function (response) {
        var x;
        var selectArray = [];

        for(x in response){
            selectArray.push(response[x].company_name);
        }

        var selectList = document.createElement("select");
        selectList.id = "select";

        for (var i = 0; i < selectArray.length; i++) {
            var option = document.createElement("option");
            option.value = selectArray[i];
            option.text = selectArray[i];
            option.id = response[i].id;
            selectList.appendChild(option);
        }

        container.appendChild(selectList);

        var compInfo = document.createElement("div");
        compInfo.className = "user_info";
        container.appendChild(compInfo);

        function getUserCompany(){
            compInfo.innerHTML = "";

            var selectId = $("#select").find('option:selected').attr('id');

            var settings = {
                "async": true,
                "crossDomain": true,
                "url": "http://localhost/~ranaterning/htdocs/book-it/api/public/api/company/"+selectId,
                "method": "GET",
                "headers": {
                    "Authorization": "Bearer "+apiToken,
                }
            }

            $.ajax(settings).done(function (response) {

                document.getElementById("page-title").innerHTML = response.company_name;

                var userCompany = response;

                var editPenIcon = document.createElement("a");
                editPenIcon.className = "icon-pencil";
                editPenIcon.id = response.id;
                var iconText = document.createTextNode("REDIGERA");
                editPenIcon.appendChild(iconText);
                compInfo.appendChild(editPenIcon);

                delete response.category_id;
                delete response.created_at;
                delete response.updated_at;
                delete response.image;

                var li = "";
                var i = 0;

                var compKeys = ["Företags id", "Företagsnamn", "E-post",
                "Beskrivning", "Stad", "Address", "Mobilnummer", "Webbsida"];

                for (var x in response) {
                    li += "<li>" + compKeys[i] +": " +response[x] + "</li>";
                    i++;
                }

                var ul = document.createElement("ul");
                compInfo.appendChild(ul);
                ul.innerHTML = li;
                editPenIcon.addEventListener("click", function(){registerCompanyForm(false, response)});
            });
        }

        getUserCompany();
        $( "#select" ).change(getUserCompany);
    });
}

//Add a company or edit a company depending on true or false
function registerCompanyForm(trueOrFalse, compInfo){
    closeMenu();
    container.innerHTML = "";
    document.getElementById("page-title").innerHTML = "";

    if(trueOrFalse) {
        document.getElementById("page-title").innerHTML = "LÄGG TILL FÖRETAG/BOKNINGSÄGARE";
    } else {
        document.getElementById("page-title").innerHTML = "REDIGERA FÖRETAG/BOKNINGSÄGARE";
    }

    $(".user-icons").find("div").css("background-color", "#f7f6f6");
    $(".icon-heart-empty").css("background-color", "#e6d5d5");

    var f = document.createElement("form");
    f.setAttribute('method',"post");
    f.id = "company_form";
    container.appendChild(f);

    var img = document.createElement("img");
    img.id = "img";
    img.src="images/placeholder.png";
    f.appendChild(img);

    var imageLabel = document.createElement("Label");
    imageLabel.htmlFor = "image";
    imageLabel.innerHTML="Bild:";

    var image = document.createElement("input");
    image.setAttribute('type',"file");
    image.setAttribute('name',"image");
    image.id = "image";
    image.accept = "image/*";
    f.appendChild(imageLabel);
    f.appendChild(image);

    $("#image").change(function(){
        var input = document.getElementById("image");
        var fReader = new FileReader();
        fReader.readAsDataURL(input.files[0]);
        imgSrc = input.files[0].name;

        fReader.onloadend = function(e){
            var img = document.getElementById("img");
            img.src = e.target.result;
        }
    });

    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "http://localhost/~ranaterning/htdocs/book-it/api/public/api/categories",
        "method": "GET",
        "headers": {
            "Content-Type": "application/json",
        }
    }

    $.ajax(settings).done(function (response) {
        var x;
        var selectArray = [];

        for(x in response){
            selectArray.push(response[x].name);
        }

        var selectList = document.createElement("select");
        selectList.id = "select";

        for (var i = 0; i < selectArray.length; i++) {
            var option = document.createElement("option");
            option.value = selectArray[i];
            option.text = selectArray[i];
            option.id = response[i].id;
            selectList.appendChild(option);
        }

        var catLabel = document.createElement("Label");
        catLabel.innerHTML="Kategori:*";
        f.appendChild(catLabel);
        f.appendChild(selectList);

        var cnameLabel = document.createElement("Label");
        cnameLabel.htmlFor = "c_name";
        cnameLabel.innerHTML="Företagsnamn/Bokningsägarens namn:*";

        var cname = document.createElement("input");
        cname.setAttribute('type',"text");
        cname.setAttribute('name',"c_name");
        cname.id = "c_name";
        f.appendChild(cnameLabel);
        f.appendChild(cname);

        var emailLabel = document.createElement("Label");
        emailLabel.htmlFor = "email";
        emailLabel.innerHTML="E-post:*";

        var email = document.createElement("input");
        email.setAttribute('type',"text");
        email.setAttribute('name',"email");
        email.id = "email";
        f.appendChild(emailLabel);
        f.appendChild(email);

        var addressLabel = document.createElement("Label");
        addressLabel.htmlFor = "address";
        addressLabel.innerHTML="Address:*";

        var address = document.createElement("input");
        address.setAttribute('type',"text");
        address.setAttribute('name',"address");
        address.id = "address";
        f.appendChild(addressLabel);
        f.appendChild(address);

        var numberLabel = document.createElement("Label");
        numberLabel.htmlFor = "number";
        numberLabel.innerHTML="Mobil nummer:";

        var number = document.createElement("input");
        number.setAttribute('type',"text");
        number.setAttribute('name',"mobile_number");
        number.id = "mobile_number";
        f.appendChild(numberLabel);
        f.appendChild(number);

        var townLabel = document.createElement("Label");
        townLabel.htmlFor = "town";
        townLabel.innerHTML="Stad:";

        var town = document.createElement("input");
        town.setAttribute('type',"text");
        town.setAttribute('name',"town");
        town.id = "town";
        f.appendChild(townLabel);
        f.appendChild(town);

        var webLabel = document.createElement("Label");
        webLabel.htmlFor = "webpage";
        webLabel.innerHTML="Webbplats:";

        var webpage = document.createElement("input");
        webpage.setAttribute('type',"text");
        webpage.setAttribute('name',"webpage");
        webpage.id = "webpage";
        f.appendChild(webLabel);
        f.appendChild(webpage);

        var descLabel = document.createElement("Label");
        descLabel.htmlFor = "description";
        descLabel.innerHTML="Beskrivning:";

        var description = document.createElement("input");
        description.setAttribute('type',"text");
        description.setAttribute('name',"description");
        description.id = "description";
        f.appendChild(descLabel);
        f.appendChild(description);

        if(trueOrFalse) {
            //Add a company
            var btn = document.createElement("button");
            btn.id = "booking-button";
            btnText = document.createTextNode("REGISTRERA");
            btn.appendChild(btnText);
            f.appendChild(btn);

            document.getElementById("booking-button").addEventListener('click', function(e){
                e.preventDefault();
                var id = $("#select").find('option:selected').attr('id');

                var data = JSON.stringify({
                    "category_id": id,
                    "company_name": $("#c_name").val(),
                    "email": $("#email").val(),
                    "mobile_number": $("#mobile_number").val(),
                    "town": $("#town").val(),
                    "address": $("#address").val(),
                    "webpage": $("#webpage").val(),
                    "description": $("#description").val(),
                    "image": imgSrc
                });

                var xhr = new XMLHttpRequest();

                xhr.open("POST", "http://localhost/~ranaterning/htdocs/book-it/api/public/api/add-company");
                xhr.setRequestHeader("Content-Type", "application/json");
                xhr.setRequestHeader("Authorization", "Bearer "+apiToken);

                xhr.send(data);
                userCompanyPage();
            });

        } else {
            //Edit company
            var btn = document.createElement("button");
            btn.id = "booking-button";
            btnText = document.createTextNode("REDIGERA");
            btn.appendChild(btnText);
            f.appendChild(btn);

            cname.value = compInfo.company_name;
            email.value = compInfo.email;
            town.value = compInfo.town;
            number.value = compInfo.mobile_number;
            address.value = compInfo.address;
            webpage.value = compInfo.webpage;
            description.value = compInfo.description;

            btn.addEventListener('click', function(event){
                event.preventDefault();
                var data = JSON.stringify({
                    "category_id": $("#category_id").val(),
                    "company_name": $("#company_name").val(),
                    "email":  $("#email").val(),
                    "town": $("#town").val(),
                    "address": $("#address").val(),
                    "image": $("#image").val(),
                    "description": $("#description").val(),
                    "mobile_number": $("#mobile_number").val(),
                    "webpage": $("#webpage").val()
                });

                var xhr = new XMLHttpRequest();

                xhr.open("POST", "http://localhost/~ranaterning/htdocs/book-it/api/public/api/update-booking-company/"+compInfo.id);
                xhr.setRequestHeader("Content-Type", "application/json");
                xhr.setRequestHeader("Authorization", "Bearer "+apiToken);

                xhr.send(data);
            });
        }

    });

}
