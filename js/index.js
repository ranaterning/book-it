window.addEventListener("load", function(e) {
    e.preventDefault();
    getCategories();
    document.getElementById("logo").addEventListener('click', getCategories);
});

//Get all categories for the startpage
function getCategories(){
    container.innerHTML = "";
    document.getElementById("user-menu").innerHTML = "";
    document.getElementById("page-title").innerHTML = "";

    //Hämtar alla kategorier
    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "http://localhost/~ranaterning/htdocs/book-it/api/public/api/categories",
        "method": "GET",
        "headers": {
        }
    }

    $.ajax(settings).done(function (response) {

        for (var i = 0; i < response.length; i++) {
            var div = document.createElement("div");
            div.id = response[i].id;
            div.className = "white-box category-box";
            container.appendChild(div);
            var url = `images/${response[i].image}`;
            document.getElementById(response[i].id).style.backgroundImage = "url("+url+")";
            var catLink = document.createElement("a");
            var linkText = document.createTextNode(response[i].name);
            catLink.className = response[i].id + " category-link";
            catLink.appendChild(linkText);
            document.getElementById(response[i].id).appendChild(catLink);

            var id = response[i].id;
            var title = response[i].name;

            catLink.onclick = (function(id,title){
               return function(){
                   getCategoryCompanies(id,title);
               }
           })(id,title);

        }
    });
}
//Get all companies belonging to a certain category
function getCategoryCompanies(id,title){
    container.innerHTML = "";
    document.getElementById("user-menu").innerHTML = "";
    document.getElementById("page-title").innerHTML = "";

    //Titel på sidan
    document.getElementById("page-title").innerHTML = title;

    var categoryBox = document.querySelectorAll(".category-box");

    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "http://localhost/~ranaterning/htdocs/book-it/api/public/api/companies/"+id,
        "method": "GET",
        "headers": {
        }
    }

    $.ajax(settings).done(function (response) {

        var x;
        for (x in response) {
            var div = document.createElement("div");
            div.className = "bookings";
            div.id = response[x].id;
            var imgDiv = document.createElement("div");
            imgDiv.className = "small-img";
            var url = `images/${response[x].image}`;
            imgDiv.style.backgroundImage = "url("+url+")";
            div.appendChild(imgDiv);
            var textBox = document.createElement("div");
            textBox.className = "text-box";
            div.appendChild(textBox);
            var strong = document.createElement("strong");
            title = document.createTextNode(response[x].company_name);
            strong.appendChild(title);
            textBox.appendChild(strong);
            var p = document.createElement("p");
            var info = document.createTextNode(response[x].town+" "+response[x].address);
            p.appendChild(info);
            textBox.appendChild(p);
            container.appendChild(div);

            var id = response[x].id;
            div.onclick = (function(id){
            return function(){
                getCompanyBookings(id,true);
            }})(id);
        }
    });

    closeMenu();
}

//Get a booking by id, either fo the company-page or for a regular user
function getBooking(id,trueOrFalse){
    container.innerHTML = "";

    var settings = {
        "async": true,
        "url": "http://localhost/~ranaterning/htdocs/book-it/api/public/api/get-booking/"+id,
        "method": "GET",
        "headers": {
        }
    }

    $.ajax(settings).done(function (response) {

        //Page title
        document.getElementById("page-title").innerHTML = response.title;

        var div = document.createElement("div");
        div.className = "booking-img";
        var url = `images/${response.image}`;
        div.style.backgroundImage = "url("+url+")";
        container.appendChild(div);

        var bookingInfo = [];
        bookingInfo.push(response);

        var bookingDiv = document.createElement("div");
        bookingDiv.className = "booking";
        var h2 = document.createElement("h2");
        var h2Text = document.createTextNode(response.title);
        h2.appendChild(h2Text);
        var textBox = document.createElement("p");
        var text = document.createTextNode(response.description);
        textBox.appendChild(text);
        bookingDiv.appendChild(h2);
        bookingDiv.appendChild(textBox);
        container.appendChild(bookingDiv);

        var personData = [];
           var dateTimeDivs = $('.date-time-div');

           for (var i = 0; i < dateTimeDivs.length; i++) {
               var inputs = $(dateTimeDivs[i]).find('input');
               personData[i] = {};

               for (var x = 0; x < inputs.length; x++) {
                   var key = $(inputs[x]).attr('name');
                   var value = $(inputs[x]).val();
                   personData[i][key] = value;
               }
           }

        var timeData = [];
        var j = 0;
        for (var x in response.booking_staff) {

            for (var i = 0; i < response.booking_staff[x].times.length; i++) {

                if(response.booking_staff[x].times[i].capacity > 0) {

                    var start_date = response.booking_staff[x].times[i].start_date;
                    var end_date = response.booking_staff[x].times[i].end_date;
                    var start_time = response.booking_staff[x].times[i].start_time;
                    var end_time = response.booking_staff[x].times[i].end_time;

                    timeData[j] = {};

                    timeData[j]["id"] = response.booking_staff[x].times[i].id;
                    timeData[j]["title"] = start_time+"-"+end_time;
                    timeData[j]["start"] = start_date+"T"+start_time;
                    timeData[j]["end"] = end_date+"T"+end_time;
                    timeData[j]["capacity"] = response.booking_staff[x].times[i].capacity;
                    j++;
                }
            }
        }

    if(timeData.length >= 1) {
        if(trueOrFalse == false){

            var addTimesLink = document.createElement("a");
            addTimesLink .innerHTML = "Lägg till tider +";
            addTimesLink.className = "page-link";
            container.appendChild(addTimesLink);
            addTimesLink.addEventListener('click', function(e){addBookingTimes(e,response,response.company.id)});
        }

        var calendarDiv = document.createElement("div");
        calendarDiv.id = "calendar";
        container.appendChild(calendarDiv);

        var months = ["01","02","03","04","05","06","07","08","09","10","11","12"];

        var date = new Date();
        var year = date.getFullYear();
        var monthIndex = date.getMonth();
        var day = date.getDate();

        var fullDate = year + "-" + months[monthIndex] + "-" + day;

        $(document).ready(function() {
          var initialLocaleCode = 'sv';

          $('#calendar').fullCalendar({
            header: {
              left: 'prev,next today',
              center: 'title',
              right: 'month,agendaWeek,agendaDay,listMonth'
            },
            defaultDate: fullDate,
            locale: initialLocaleCode,
            buttonIcons: false,
            weekNumbers: true,
            navLinks: true,
            editable: true,
            eventLimit: true,
            events: timeData,
            eventClick: function(event) {
               if (event) {
                   if(trueOrFalse) {
                       reserveBooking(event.id, response, true);
                   } else {
                       reserveBooking(event.id, response, false);
                   }

               }
             }
          });

        });
    } else if (trueOrFalse) {
        var emptyH1 = document.createElement("h1");
        emptyH1.innerHTML = "INGA LEDIGA TIDER";
        container.appendChild(emptyH1);
    } else {
        var addTimesLink = document.createElement("a");
        addTimesLink .innerHTML = "Lägg till tider +";
        addTimesLink.className = "page-link";
        container.appendChild(addTimesLink);
        addTimesLink.addEventListener('click', function(e){addBookingTimes(e,response,response.company.id)});

        var emptyH1 = document.createElement("h1");
        emptyH1.innerHTML = "INGA TIDER FÖR "+response.title;
        container.appendChild(emptyH1);
    }

    });
}
