//Function for adding times to a booking
function addBookingTimes(e, bookingResponse, compId){
    if(bookingResponse instanceof Object) {
        var booking = bookingResponse;
    } else {
        var booking = JSON.parse(bookingResponse);
    }

    container.innerHTML = "";
    document.getElementById("page-title").innerHTML = "LÄGG TILL TIDER";

    var calendarContainer = document.createElement("div");
    calendarContainer.id = "calendar";
    container.appendChild(calendarContainer);

    var timeAndDateArray = [];
    var initialLocaleCode = 'sv';
    $('#calendar').fullCalendar({
        locale: initialLocaleCode,
        selectable: true,
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay'
        },
        dayClick: function(date) {

            var background = document.createElement("div");
            background.className = "background";
            background.style.display = "block";
            document.getElementById("body").appendChild(background);

            var dateTimeDiv = document.createElement("div");
            dateTimeDiv.className = "add-date-time";
            background.appendChild(dateTimeDiv);

            var smallDiv = document.createElement("div");
            smallDiv.className = "date-time-div";
            dateTimeDiv.appendChild(smallDiv);

            var settings = {
                "async": true,
                "crossDomain": true,
                "url": "http://localhost/~ranaterning/htdocs/book-it/api/public/api/company-staff/"+compId,
                "method": "GET",
                "headers": {
                    "Content-Type": "application/json",
                    "Authorization": "Bearer "+apiToken,
                }
            }

            $.ajax(settings).done(function (response) {
                var x;
                var selectArray = [];

                for(x in response.company_staff){
                    selectArray.push(response.company_staff[x].f_name+" "+response.company_staff[x].l_name);
                }

                var selectLabel = document.createElement("Label");
                selectLabel.htmlFor = "select";
                selectLabel.innerHTML="Personal:*";

                var selectList = document.createElement("select");
                selectList.id = "select";

                for (var i = 0; i < selectArray.length; i++) {
                    var option = document.createElement("option");
                    option.value = selectArray[i];
                    option.text = selectArray[i];
                    option.id = response.company_staff[i].id;
                    selectList.appendChild(option);
                }

                smallDiv.appendChild(selectLabel);
                var plus = document.createElement("p");
                plus.className = "plus";
                var plusSign = document.createTextNode("Lägg till personal  +");
                plus.appendChild(plusSign);
                plus.addEventListener('click', function(){registerForm(false)});
                smallDiv.appendChild(plus);
                smallDiv.appendChild(selectList);

                var startDateInputDiv = document.createElement("div");
                startDateInputDiv.className = "time-inputs";
                smallDiv.appendChild(startDateInputDiv);

                var endDateInputDiv = document.createElement("div");
                endDateInputDiv.className = "time-inputs";
                smallDiv.appendChild(endDateInputDiv);

                var capacityLabel = document.createElement("Label");
                capacityLabel.htmlFor = "capacity";
                capacityLabel.innerHTML="Tid-kapacitet:";

                var capacity = '<span id="minus"> - </span><input value="1" id="capacity" type="text" name="capacity"><span id="plus"> + </span>';
                startDateInputDiv.appendChild(capacityLabel);
                startDateInputDiv.innerHTML += capacity;

                document.getElementById('plus').addEventListener('click', plusFunction);
                document.getElementById('minus').addEventListener('click', minusFunction);

                var start_dateLabel = document.createElement("Label");
                start_dateLabel .htmlFor = "start_date";
                start_dateLabel .innerHTML="Startdatum:*";

                var start_date = document.createElement("input");
                start_date.setAttribute('type',"text");
                start_date.setAttribute('name',"start_date");
                start_date.value = date.format();
                start_date.className = "datepicker times";
                startDateInputDiv.appendChild(start_dateLabel);
                startDateInputDiv.appendChild(start_date);

                var end_dateLabel = document.createElement("Label");
                end_dateLabel.htmlFor = "end_date";
                end_dateLabel.innerHTML="Slutdatum:*";

                var end_date = document.createElement("input");
                end_date.setAttribute('type',"text");
                end_date.setAttribute('name',"end_date");
                end_date.className = "datepicker times";
                end_date.value = date.format();
                endDateInputDiv.appendChild(end_dateLabel);
                endDateInputDiv.appendChild(end_date);

                var startTimeLabel = document.createElement("Label");
                startTimeLabel.htmlFor = "start_time";
                startTimeLabel.innerHTML="Starttid:*";

                var startTime = document.createElement("input");
                startTime.setAttribute('type',"text");
                startTime.setAttribute('name',"start_time");
                startTime.className = "times";
                smallDiv.appendChild(startDateInputDiv);
                startDateInputDiv.appendChild(startTimeLabel);
                startDateInputDiv.appendChild(startTime);

                var endTimeLabel = document.createElement("Label");
                endTimeLabel.htmlFor = "end_time";
                endTimeLabel.innerHTML="Sluttid:*";

                var endTime = document.createElement("input");
                endTime.setAttribute('type',"text");
                endTime.setAttribute('name',"end_time");
                endTime.className = "times";
                smallDiv.appendChild(endDateInputDiv);
                endDateInputDiv.appendChild(endTimeLabel);
                endDateInputDiv.appendChild(endTime);

                var btn = document.createElement("button");
                btn.className = "add-time-buttons";
                btnText = document.createTextNode("LÄGG TILL");
                btn.appendChild(btnText);
                smallDiv.appendChild(btn);

                var cancelBtn = document.createElement("button");
                cancelBtnText = document.createTextNode("AVBRYT");
                cancelBtn.appendChild(cancelBtnText);
                cancelBtn.className = "add-time-buttons cancel-button";
                cancelBtn.addEventListener("click", function() {background.style.display = "none"; });
                smallDiv.appendChild(cancelBtn);

                $(btn).on('click', function (e) {
                    e.preventDefault();
                    var staffId = $("#select").find('option:selected').attr('id');

                    var timeData = [];

                    var dateTimeDivs = $('.date-time-div');

                    for (var i = 0; i < dateTimeDivs.length; i++) {
                        var inputs = $(dateTimeDivs[i]).find('input');
                        timeData[i] = {};

                        for (var x = 0; x < inputs.length; x++) {
                            var key = $(inputs[x]).attr('name');
                            var value = $(inputs[x]).val();
                            timeData[i][key] = value;
                            timeData[i]['company_id'] = compId;
                            timeData[i]['booking_id'] = booking.id;
                        }
                    }

                    timeData[0]['user_id'] = staffId;

                    var data = JSON.stringify({
                      "time": timeData
                    });

                    var xhr = new XMLHttpRequest();

                    xhr.addEventListener("readystatechange", function () {
                      if (this.readyState === 4) {
                          background.style.display = "none";
                      }
                    });

                    xhr.open("POST", "http://localhost/~ranaterning/htdocs/book-it/api/public/api/add-booking-times");
                    xhr.setRequestHeader("Content-Type", "application/json");
                    xhr.setRequestHeader("Authorization", "Bearer "+apiToken);

                    xhr.send(data);

                });
            });
        }
    });
}
