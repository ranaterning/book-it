window.addEventListener("load", function(e) {
   e.preventDefault();
   getNavInfo();
   document.querySelector(".menu").addEventListener('click', openMenu);
   document.querySelector('.close-menu').addEventListener('click', closeMenu);
   document.querySelector('.menu-bg').addEventListener('click', closeMenu);
 });

function openMenu(e){
   //Opens the menu
   $( ".menu-bg" ).fadeIn();
   $( ".main-menu" ).fadeIn();

   document.querySelector('.menu').style.display = "none";
   document.querySelector('.close-menu').style.display = "block";
}

function getNavInfo(){
   //Getting and displaying the categories in the menu
   var settings = {
     "async": true,
     "crossDomain": true,
     "url": "http://localhost/~ranaterning/htdocs/book-it/api/public/api/categories",
     "method": "GET",
     "headers": {
     }
   }

   $.ajax(settings).done(function (response) {

   var ul = document.createElement("ul");
   document.getElementById("nav").appendChild(ul);

   var userPageLi = document.createElement("li");
   userPageText = document.createTextNode("Mina sidor");
   userPageLi.appendChild(userPageText);
   ul.appendChild(userPageLi);

 if(userId){
   userPageLi.addEventListener('click', userPage);
   var userLevel = readCookie('user_level');

   switch (userLevel) {
       case 'company':
       var uli = document.createElement("li");
       var uliText = document.createTextNode("Mina företag");
       uli.appendChild(uliText);
       ul.appendChild(uli);
       uli.addEventListener('click', userCompanyPage);

       break;

       case 'staff':
       var uli = document.createElement("li");
       var uliText = document.createTextNode("MITT SCHEMA");
       uli.appendChild(uliText);
       ul.appendChild(uli);
       uli.addEventListener('click', getPersonalSchedule);
       default:
   }

} else {
   userPageLi.addEventListener('click', function(e){loginForm(e)});
}

   var catLi = document.createElement("li");
   catStrong = document.createElement("strong");
   catText = document.createTextNode("Kategorier");
   catStrong.appendChild(catText);
   catLi.appendChild(catStrong);
   ul.appendChild(catLi);

       for (var i = 0; i < response.length; i++) {

           var navLi = document.createElement("li");
           var liText = document.createTextNode(response[i].name);
           navLi.className = response[i].id;
           navLi.appendChild(liText);
           navLi.className = "weight";
           ul.appendChild(navLi);

           var id = response[i].id;
           var title = response[i].name;

           navLi.onclick = (function(id,title){
              return function(){
                  getCategoryCompanies(id,title);
              }
          })(id,title);
       }
   });
}

function closeMenu(){
   //Closing the menu
   document.querySelector('.main-menu').style.display = "none";
   document.querySelector('.menu').style.display = "block";
   document.querySelector('.close-menu').style.display = "none";
   document.querySelector('.menu-bg').style.display = "none";
}
