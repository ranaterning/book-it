window.addEventListener("load", function(event) {

   event.preventDefault();
   checkIfLoggedIn();

   if(readCookie('user_id')){
       document.getElementById("log-in").addEventListener('click', userPage);
   } else {
       document.getElementById("log-in").addEventListener('click', function(e){loginForm(e)});
   }
 });

//Helpfunctions for creating reading and deleting cookies
function createCookie(name,value,days) {
   if (days) {
       var date = new Date();
       date.setTime(date.getTime()+(days*24*60*60*1000));
       var expires = "; expires="+date.toGMTString();
   }
   else var expires = "";
   document.cookie = name+"="+value+expires+"; path=/";
}

function readCookie(name) {
   var nameEQ = name + "=";
   var ca = document.cookie.split(';');
   for(var i=0;i < ca.length;i++) {
       var c = ca[i];
       while (c.charAt(0)==' ') c = c.substring(1,c.length);
       if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
   }
   return null;
}

function deleteCookies() {
   document.cookie.split(";").forEach(function(c) {
       document.cookie = c.replace(/^ +/, "").replace(/=.*/, "=;expires=" + new Date().toUTCString() + ";path=/");
   });
}

//Login form
function loginForm(e) {
   e.preventDefault();
   closeMenu();

   container.innerHTML = "";
   document.getElementById("user-menu").innerHTML = "";
   document.getElementById("page-title").innerHTML = "LOGGA IN";

   var f = document.createElement("form");
   f.id = "login";

   var regP = document.createElement("p");
   f.appendChild(regP);
   regP.innerHTML = "Inte medlem? <a class='register_link' href='#'>Registrera dig in här</a>";
   regP.addEventListener('click', function(){registerForm(true)});

   var emailLabel = document.createElement("Label");
   emailLabel.htmlFor = "email";
   emailLabel.innerHTML="E-post:";

   var email = document.createElement("input");
   email.setAttribute('type',"text");
   email.setAttribute('name',"email");
   email.id = "email";
   f.appendChild(emailLabel);
   f.appendChild(email);

   var passwordLabel = document.createElement("Label");
   passwordLabel.htmlFor = "password";
   passwordLabel.innerHTML="Lösenord:";

   var password = document.createElement("input");
   password.setAttribute('type',"password");
   password.setAttribute('name',"password");
   password.id = "password";
   f.appendChild(passwordLabel);
   f.appendChild(password);

   var btn = document.createElement("button");
   btn.id = "login-button";
   btnText = document.createTextNode("Logga in");
   btn.appendChild(btnText);

   f.appendChild(btn);
   container.appendChild(f);

   $(function(e){
       $(btn).click(function(e){
       e.preventDefault();
           data = JSON.stringify({
             "email": $("#email").val(),
             "password": $("#password").val()
           });
           var xhr = new XMLHttpRequest();

           xhr.addEventListener("readystatechange", function () {

             if (this.readyState === 4) {

               if(xhr.status === 200) {

                   var loginObj = JSON.parse(this.responseText);

                   var apiToken = loginObj.user_data.api_token;
                   var userId = loginObj.user_data.id;
                   var userLevel = loginObj.user_data.user_level;

                   createCookie('user_id',userId,7);
                   createCookie('api_token',apiToken,7);
                   createCookie('user_level',userLevel,7);

                   location.reload();
               } else {
                   var failMessage = document.createElement("p");
                   failMessage.className = "fail-message";
                   message = document.createTextNode("Något gick fel, försök igen.");
                   failMessage.appendChild(message);
                   f.appendChild(failMessage);
               }

             }
           });

           xhr.open("POST", "http://localhost/~ranaterning/htdocs/book-it/api/public/api/login");
           xhr.setRequestHeader("Content-Type", "application/json");

           xhr.send(data);
      });
   });
}


function checkIfLoggedIn() {
   var userId = readCookie('user_id');
   var apiToken = readCookie('api_token');

   if(userId){
       var a = document.createElement("a");
       a.innerHTML = "";
       var t = document.createTextNode("LOGGA UT");
       a.setAttribute('href', "#");
       a.id = "log-out";
       a.appendChild(t);
       document.querySelector(".icon-box").appendChild(a);
       a.addEventListener('click', logOut);
   }
}

//Logout function
function logOut(){
document.getElementById("log-out").style.display = "none";

deleteCookies();

var settings = {
 "async": true,
 "crossDomain": true,
 "url": "http://localhost/~ranaterning/htdocs/book-it/api/public/api/logout",
 "method": "POST",
}

$.ajax(settings).done(function (response) {
 location.reload();
});
}
