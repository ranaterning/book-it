//Get all reservations for the logged in user
function getUserReservations() {
	closeMenu();
    container.innerHTML = "";

	$(".user-icons").find("div").css("background-color", "#f7f6f6");
	$(".icon-clock").css("background-color", "#e6d5d5");

	var settings = {
	  "async": true,
	  "crossDomain": true,
	  "url": "http://localhost/~ranaterning/htdocs/book-it/api/public/api/user-bookings",
	  "method": "GET",
	  "headers": {
	    "Content-Type": "application/json",
	    "Authorization": "Bearer "+apiToken
	  }
	}

	$.ajax(settings).done(function (response) {
		if(response.length > 0){
			document.getElementById("page-title").innerHTML = "DINA BOKNINGAR";
		} else {
			document.getElementById("page-title").innerHTML = "INGA BOKNINGAR ÄN"
		}

		for (var i = 0; i < response.length; i++) {
			var bookingsDiv = document.createElement("div");
			bookingsDiv.className = "bookings";

			bookingsDiv.id = response[i].id;
			var imgDiv = document.createElement("div");
			imgDiv.className = "small-img";
			var url = `images/${response[i].image}`;
			imgDiv.style.backgroundImage = "url("+url+")";
			bookingsDiv.appendChild(imgDiv);

			var textBox = document.createElement("div");
			textBox.className = "text-box";
			bookingsDiv.appendChild(textBox);
			var strong = document.createElement("strong");
			title = document.createTextNode(response[i].title);
			strong.appendChild(title);
			textBox.appendChild(strong);
			var p = document.createElement("p");
			price = document.createTextNode(response[i].price+" kr");
			p.appendChild(price);
			textBox.appendChild(p);
			container.appendChild(bookingsDiv);

			bookingsDiv.addEventListener('click', getUserReservation);
		}
	});
}

//Get a reservation for the logged in user
function getUserReservation(){
	var settings = {
  "async": true,
  "crossDomain": true,
  "url": "http://localhost/~ranaterning/htdocs/book-it/api/public/api/user-booking/"+this.id,
  "method": "GET",
  "headers": {
    "Content-Type": "application/json",
    "Authorization": "Bearer "+apiToken
  }
}

$.ajax(settings).done(function (response) {
	container.innerHTML = "";
	document.getElementById("page-title").innerHTML = "";
	document.getElementById("page-title").innerHTML = response[0].title;

	var img = document.createElement("div");
	img.className = "booking-img";
	var url = `images/${response[0].image}`;
	img.style.backgroundImage = "url("+url+")";
	container.appendChild(img);

	var personDiv = document.createElement("div");
	personDiv.className = "person-div";
	container.appendChild(personDiv);
	//Div för bokningsinfo
	var bookingInfoDiv = document.createElement("div");
	bookingInfoDiv.className = "booking-info";
	bookingInfoDiv.id = "booking-info";
	container.appendChild(bookingInfoDiv);

	var ul = document.createElement("ul");
	ul.id = 'info';

	var url = `images/${response[0].image}`;
	var name = response.booking_staff[0].f_name+"-"+response.booking_staff[0].l_name;
	var person = "<div class='small-img' style='background-image:url("+url+")'> </div> <a class='cancel' href='#'>Avboka</a> <li> <strong>Utövare</strong> <div> <p>"+name+"</p></div> </li>";
	personDiv.innerHTML = person;

	bookingInfoDiv.appendChild(personDiv);
	bookingInfoDiv.appendChild(ul);

	 document.getElementsByClassName('cancel')[0].addEventListener('click', function(e){
		 e.preventDefault();
		if (window.confirm("Är du säker på att du vill avboka "+response[0].title+"?")){
			    var data = null;
				var xhr = new XMLHttpRequest();
				xhr.withCredentials = true;

				xhr.addEventListener("readystatechange", function () {
				  if (this.readyState === 4) {
				  }
				});

				xhr.open("GET", "http://localhost/~ranaterning/htdocs/book-it/api/public/api/cancel-booking/"+response.user_booking.id);
				xhr.setRequestHeader("Authorization", "Bearer "+apiToken);
				xhr.send(data);
				getUserReservations();
		}
	});

	var info = [response.company[0].company_name, response[0].title, response.company[0].address, response[0].price];
	var titles = ['Företag','Tjänst:', 'Address:', 'Pris:', 'Tid:', 'Datum:'];

	time = response.booking_staff.time[0].start_time+"-"+response.booking_staff.time[0].end_time;
	info.push(time, response.booking_staff.time[0].start_date);

	var text = "";
	for (var i = 0; i < info.length; i++ ) {
			text += "<li> <strong>"+titles[i]+"</strong> <div><p>"+info[i]+"</p></div></li>";
	}
	document.getElementById("info").innerHTML = text;

	var mapDiv = document.createElement("div");
	mapDiv.className = "booking-info";
	mapDiv.id = "booking-info";
	container.appendChild(mapDiv);
	var map = document.createElement("div");
	map.id = "map";
	mapDiv.appendChild(map);
  	var s = document.createElement( 'script' );
	var src = "https://maps.googleapis.com/maps/api/js?key=AIzaSyDtaXm2i9HtKA6MKfQMgLK7idDPvJwljfs&callback=initMap";
    s.setAttribute( 'src', src );
    document.body.appendChild( s );
	address = response.company[0].address;

});
}

function initMap() {
	 geocoder = new google.maps.Geocoder();
	 if (geocoder) {
		 geocoder.geocode({
				 'address': address
		 }, function (results, status) {
				 if (status == google.maps.GeocoderStatus.OK) {
						 showResult(results[0]);
				 }
		 });
	}

	function showResult(result) {
			var uluru = {lat: result.geometry.location.lat(), lng: result.geometry.location.lng()};
			var map = new google.maps.Map(document.getElementById('map'), {
				zoom: 15,
				center: uluru
			});
			var marker = new google.maps.Marker({
				position: uluru,
				map: map
			});
	}

 }
