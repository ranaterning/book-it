//Register form for either adding new users as a company or signing up
function registerForm(isTrueOrFalse){

	closeMenu();
	if(document.querySelector(".background")) {
		document.querySelector(".background").style.display = "none";
	}
	container.innerHTML = "";

	var f = document.createElement("form");
    f.id = "login";

	if(isTrueOrFalse){
		document.getElementById("user-menu").innerHTML = "";
		document.getElementById("page-title").innerHTML = "REGISTRERA DIG";

		var regP = document.createElement("p");
		f.appendChild(regP);
		regP.innerHTML = "Medlem? <a class='login_link' href=''>Logga in här</a>";
		regP.addEventListener('click',function(e){loginForm(e)});
		container.appendChild(f);

		$(function(e){
			$(btn).click(function(e){
			e.preventDefault();

			data = JSON.stringify({
			  "f_name": $('#f_name').val(),
			  "l_name": $('#l_name').val(),
			  "email": $('#email').val(),
			  'town': $('#town').val(),
			  'address': $('#address').val(),
			  'zip': $('#zip').val(),
			  'mobile_number': $('#mobile_number').val(),
			  'password': $('#password').val(),
			  'password_confirmation': $('#password_confirmation').val()
			});

			var xhr = new XMLHttpRequest();

			xhr.addEventListener("readystatechange", function () {
			  if (this.readyState === 4) {
			  }
			});

			xhr.open("POST", "http://localhost/~ranaterning/htdocs/book-it/api/public/api/register");
			xhr.setRequestHeader("Content-Type", "application/json");

			xhr.send(data);

			loginForm(e);
		});
	 });

	} else {

		$(".user-icons").find("div").css("background-color", "#f7f6f6");
		$(".icon-user").css("background-color", "#e6d5d5");
		document.getElementById("page-title").innerHTML = "REGISTRERA PERSONAL";

		var settings = {
	      "async": true,
	      "crossDomain": true,
	      "url": "http://localhost/~ranaterning/htdocs/book-it/api/public/api/user-companies/"+userId,
	      "method": "GET",
	      "headers": {
	        "Authorization": "Bearer "+apiToken
	      }
	    }

	    $.ajax(settings).done(function (response) {
	        var x;
	        var selectArray = [];

	        for(x in response){
	            selectArray.push(response[x].company_name);
	        }

	        var selectList = document.createElement("select");
	        selectList.id = "select";

	        for (var i = 0; i < selectArray.length; i++) {
	          var option = document.createElement("option");
	          option.value = selectArray[i];
	          option.text = selectArray[i];
	          option.id = response[i].id;
	          selectList.appendChild(option);
	        }

			//Appenda högst upp på något bättre sätt.
	        container.appendChild(selectList);
			container.appendChild(f);

			var selectedCompanyId = $("#select").find('option:selected').attr('id');

			$(function(e){
				$(btn).click(function(e){
				e.preventDefault();

				data = JSON.stringify({
				  "f_name": $('#f_name').val(),
				  "l_name": $('#l_name').val(),
				  "email": $('#email').val(),
				  'town': $('#town').val(),
				  'address': $('#address').val(),
				  'zip': $('#zip').val(),
				  'mobile_number': $('#mobile_number').val(),
				  'password': $('#password').val(),
				  'password_confirmation': $('#password_confirmation').val()
				});

				var xhr = new XMLHttpRequest();

				xhr.addEventListener("readystatechange", function () {
				  if (this.readyState === 4) {
					var userObj = JSON.parse(this.responseText);
					var data = new FormData();
					data.append("company_id", selectedCompanyId);
					data.append("user_id", userObj[0].id);

					var xhr = new XMLHttpRequest();

					xhr.addEventListener("readystatechange", function () {
					  if (this.readyState === 4) {
					  }
					});

					xhr.open("POST", "http://localhost/~ranaterning/htdocs/book-it/api/public/api/add-company-user");
					xhr.setRequestHeader("Authorization", "Bearer "+apiToken);

					xhr.send(data);

					userCompanyPage();
				  }
				});

				xhr.open("POST", "http://localhost/~ranaterning/htdocs/book-it/api/public/api/register");
				xhr.setRequestHeader("Content-Type", "application/json");

				xhr.send(data);

			});
		 });

		});
	}

    var fnameLabel = document.createElement("Label");
    fnameLabel.htmlFor = "f_name";
    fnameLabel.innerHTML="Förnamn:*";

    var fname = document.createElement("input");
    fname.setAttribute('type',"text");
    fname.setAttribute('name',"f_name");
    fname.id = "f_name";
    f.appendChild(fnameLabel);
    f.appendChild(fname);

    var lnameLabel = document.createElement("Label");
    lnameLabel.htmlFor = "l_name";
    lnameLabel.innerHTML="Efternamn:*";

    var lname = document.createElement("input");
    lname.setAttribute('type',"text");
    lname.setAttribute('name',"l_name");
    lname.id = "l_name";
    f.appendChild(lnameLabel);
    f.appendChild(lname);

    var emailLabel = document.createElement("Label");
    emailLabel.htmlFor = "email";
    emailLabel.innerHTML="E-post:*";

    var email = document.createElement("input");
    email.setAttribute('type',"text");
    email.setAttribute('name',"email");
    email.id = "email";
    f.appendChild(emailLabel);
    f.appendChild(email);

    var numberLabel = document.createElement("Label");
    numberLabel.htmlFor = "number";
    numberLabel.innerHTML="Mobil nummer:";

    var number = document.createElement("input");
    number.setAttribute('type',"text");
    number.setAttribute('name',"mobile_number");
    number.id = "mobile_number";
    f.appendChild(numberLabel);
    f.appendChild(number);

    var townLabel = document.createElement("Label");
    townLabel.htmlFor = "town";
    townLabel.innerHTML="Stad:";

    var town = document.createElement("input");
    town.setAttribute('type',"text");
    town.setAttribute('name',"town");
    town.id = "town";
    f.appendChild(townLabel);
    f.appendChild(town);

    var zipLabel = document.createElement("Label");
    zipLabel.htmlFor = "zip";
    zipLabel.innerHTML="Postnummer:";

    var zip = document.createElement("input");
    zip.setAttribute('type',"text");
    zip.setAttribute('name',"zip");
    zip.id = "zip";
    f.appendChild(zipLabel);
    f.appendChild(zip);

    var addressLabel = document.createElement("Label");
    addressLabel.htmlFor = "address";
    addressLabel.innerHTML="Address:";

    var address = document.createElement("input");
    address.setAttribute('type',"text");
    address.setAttribute('name',"address");
    address.id = "address";
    f.appendChild(addressLabel);
    f.appendChild(address);

    var passwordLabel = document.createElement("Label");
    passwordLabel.htmlFor = "password";
    passwordLabel.innerHTML="Lösenord:";

    var password = document.createElement("input");
    password.setAttribute('type',"password");
    password.setAttribute('name',"password");
    password.id = "password";
    f.appendChild(passwordLabel);
    f.appendChild(password);

    var passwordConfLabel = document.createElement("Label");
    passwordConfLabel.htmlFor = "password_confirmation";
    passwordConfLabel.innerHTML="Bekräfta lösenord:";

    var passwordConf = document.createElement("input");
    passwordConf.setAttribute('type',"password");
    passwordConf.setAttribute('name',"password_confirmation");
    passwordConf.id = "password_confirmation";
    f.appendChild(passwordConfLabel);
    f.appendChild(passwordConf);

    var btn = document.createElement("button");
    btn.id = "register-button";
    btnText = document.createTextNode("REGISTRERA");
    btn.appendChild(btnText);

    f.appendChild(btn);

}
