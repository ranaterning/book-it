//reserve a booking appointment
function reserveBooking(timeId, bookingInfo,trueOrFalse){
	$("#calendar").html("");

	var userId = readCookie('user_id');
	var apiToken = readCookie('api_token');

	var bookingInfoDiv = document.createElement("div");
	bookingInfoDiv.className = "booking-info";
	bookingInfoDiv.id = "booking-info";
	container.appendChild(bookingInfoDiv);
	if(trueOrFalse) {
		var userInfoDiv = document.createElement("div");
		userInfoDiv.className = "booking-info";
		userInfoDiv.id = "user-info";
		container.appendChild(userInfoDiv);
	}

	//Bookinginfo div
	var ul = document.createElement("ul");
	ul.id = 'info';
	bookingInfoDiv.appendChild(ul);

	var info = [bookingInfo.title, bookingInfo.company.address, bookingInfo.price];
	var titles = ['Tjänst:', 'Address:', 'Pris:', 'Tid:', 'Datum:', 'Utövare:'];

	for(var x in bookingInfo.booking_staff) {

		for (var j = 0; j < bookingInfo.booking_staff[x].times.length; j++) {

			if(bookingInfo.booking_staff[x].times[j].id == timeId) {

				time = bookingInfo.booking_staff[x].times[j].start_time+"-"+bookingInfo.booking_staff[x].times[j].end_time;
				name = bookingInfo.booking_staff[x].f_name+" "+ bookingInfo.booking_staff[x].l_name;
				info.push(time, bookingInfo.booking_staff[x].times[j].start_date, name);

			}

		}
	}

   	var text = "";
    for (var i = 0; i < info.length; i++ ) {
        text += "<li> <strong>"+titles[i]+"</strong> <div><p>"+info[i]+"</p></div></li>";
    }

    document.getElementById("info").innerHTML = text;
	if(trueOrFalse) {
		$( ".booking-info" ).removeClass( "booking-info-company-view" );
		//Form for the userinfo
		var f = document.createElement("form");
		f.setAttribute('method',"post");
		f.id = "booking-form";

		var fnameLabel = document.createElement("Label");
		fnameLabel.htmlFor = "f_name";
		fnameLabel.innerHTML="Förnamn:*";

		var fname = document.createElement("input");
		fname.setAttribute('type',"text");
		fname.setAttribute('name',"f_name");
		fname.id = "f_name";
		f.appendChild(fnameLabel);
		f.appendChild(fname);

		var lnameLabel = document.createElement("Label");
		lnameLabel.htmlFor = "l_name";
		lnameLabel.innerHTML="Efternamn:*";

		var lname = document.createElement("input");
		lname.setAttribute('type',"text");
		lname.setAttribute('name',"l_name");
		lname.id = "l_name";
		f.appendChild(lnameLabel);
		f.appendChild(lname);

		var emailLabel = document.createElement("Label");
		emailLabel.htmlFor = "email";
		emailLabel.innerHTML="E-post:*";

		var email = document.createElement("input");
		email.setAttribute('type',"text");
		email.setAttribute('name',"email");
		email.id = "email";
		f.appendChild(emailLabel);
		f.appendChild(email);

		var numberLabel = document.createElement("Label");
		numberLabel.htmlFor = "number";
		numberLabel.innerHTML="Mobil nummer:";

		var number = document.createElement("input");
		number.setAttribute('type',"text");
		number.setAttribute('name',"mobile_number");
		number.id = "mobile_number";
		f.appendChild(numberLabel);
		f.appendChild(number);

		var messageLabel = document.createElement("Label");
		messageLabel.htmlFor = "message";
		messageLabel.innerHTML="Meddelande:";

		var message = document.createElement("input");
		message.setAttribute('name',"message");
		message.id = "message";
		f.appendChild(messageLabel);
		f.appendChild(message);

		var btn = document.createElement("button");
		btn.id = "booking-button";
		btn.className = bookingInfo.id;
		btnText = document.createTextNode("BOKA TID");
		btn.appendChild(btnText);

		f.appendChild(btn);
		document.getElementById("user-info").appendChild(f);

		if(userId){
			var settings = {
			  "async": true,
			  "crossDomain": true,
			  "url": "http://localhost/~ranaterning/htdocs/book-it/api/public/api/user/"+userId,
			  "method": "GET",
			  "headers": {
				"Authorization": "Bearer "+apiToken
			  }
			}
			$.ajax(settings).done(function (response) {
				 fname.value = response.f_name;
				 lname.value = response.l_name;
				 email.value = response.email;
				 number.value = response.mobile_number;
			});
		} else {

			document.getElementById("user-info").innerHTML = "";

			var h2 = document.createElement("h2");
			var h2Text = document.createTextNode("Du måste vara inloggad för att boka tid.");
			h2.appendChild(h2Text);

			var aLogIn = document.createElement("a");
			aLogIn.setAttribute("href", "#");
			aLogIn.className = "login_link";
			var aLoginText = document.createTextNode("Logga in");
			aLogIn.appendChild(aLoginText);

			var aRegister = document.createElement("a");
			aRegister.setAttribute("href", "#");
			aRegister.className = "register_link";
			var aRegisterText = document.createTextNode("här");
			aRegister.appendChild(aRegisterText);

			var p = document.createElement("p");
			var pText = document.createTextNode(" eller registrera dig ");

			p.appendChild(aLogIn);
			p.appendChild(pText);
			p.appendChild(aRegister);
			document.getElementById("user-info").appendChild(h2);
			document.getElementById("user-info").appendChild(p);

			aLogIn.addEventListener('click', function(e){loginForm(e)});
			aRegister.addEventListener('click', registerForm);
		}

		$(function(){
			$('#booking-button').click(function(event){
				$('#booking-form').submit(false);

				var data = JSON.stringify({
					"user_id": userId,
					"f_name": $("#f_name").val(),
					"l_name": $("#l_name").val(),
					"email": $("#email").val(),
					"mobile_number": $("#mobile_number").val(),
					"message": $("#message").val(),
						"time": [
						{ "booking_time_id":  timeId}
						]
				});

				var xhr = new XMLHttpRequest();
				xhr.addEventListener("readystatechange", function () {
				  if (this.readyState === 4) {
						var response = JSON.parse(this.responseText);
						thankYouPage(response);
				  }
				});

				xhr.open("POST", "http://localhost/~ranaterning/htdocs/book-it/api/public/api/reserve-booking/"+this.className);
				xhr.setRequestHeader("Content-Type", "application/json");
				xhr.setRequestHeader("Authorization", "Bearer "+apiToken);
				xhr.send(data);


		   });
		});
	} else {
		$( ".booking-info" ).addClass( "booking-info-company-view" );
	}

}

function thankYouPage(response){
	container.innerHTML = "";
	document.getElementById("user-menu").innerHTML = "";
	document.getElementById("page-title").innerHTML = "";




	document.getElementById("page-title").innerHTML = "TACK FÖR DIN BOKNING";

	var thankyouDiv = document.createElement("div");
	thankyouDiv.className = "thanks-page";
	thankyouDiv.id = "thanks-page";
	container.appendChild(thankyouDiv);

	var bookingInfoDiv = document.createElement("div");
	bookingInfoDiv.className = "booking-info";
	bookingInfoDiv.id = "booking-info";
	thankyouDiv.appendChild(bookingInfoDiv);

	var ul = document.createElement("ul");
	ul.id = 'info';
	bookingInfoDiv.appendChild(ul);

	var info = [response.id, response.created_at, response.title];
	var titles = ['Bokningsid:', 'Orderdatum:', 'Tjänst:', 'Tid:', 'Datum', 'Utövare'];

	time = response.reservation.booking_staff.time[0].start_time+"-"+response.reservation.booking_staff.time[0].end_time;
	name = response.reservation.booking_staff[0].f_name+" "+response.reservation.booking_staff[0].l_name;
	info.push(time, response.reservation.booking_staff.time[0].start_date, name);

	var text = "";
	for (var i = 0; i < info.length; i++ ) {
			text += "<li> <strong>"+titles[i]+"</strong> <div><p>"+info[i]+"</p></div></li>";
	}

	document.getElementById("info").innerHTML = text;

	var btn = document.createElement("button");
	btnText = document.createTextNode("MINA BOKNINGAR");
	btn.appendChild(btnText);
	btn.id = response.id;
	bookingInfoDiv.appendChild(btn);
	btn.addEventListener('click', getUserReservations);
}
