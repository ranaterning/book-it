//Get company bookings
function getCompanyBookings(id, trueOrFalse) {
    container.innerHTML = "";

    $(".user-icons").find("div").css("background-color", "#f7f6f6");
    $(".icon-calendar-empty").css("background-color", "#e6d5d5");

    if(trueOrFalse == false) {

        var addTimesLink = document.createElement("a");
        addTimesLink .innerHTML = "Lägg till ny bokning +";
        addTimesLink.className = "page-link";
        container.appendChild(addTimesLink);
        addTimesLink.addEventListener('click', addBookingForm);

        var settings = {
          "async": true,
          "crossDomain": true,
          "url": "http://localhost/~ranaterning/htdocs/book-it/api/public/api/user-companies/"+userId,
          "method": "GET",
          "headers": {
            "Authorization": "Bearer "+apiToken
          }
        }

        $.ajax(settings).done(function (response) {
            var x;
            var selectArray = [];

            for(x in response){
                selectArray.push(response[x].company_name);
            }

            var selectList = document.createElement("select");
            selectList.id = "select";

            for (var i = 0; i < selectArray.length; i++) {
              var option = document.createElement("option");
              option.value = selectArray[i];
              option.text = selectArray[i];
              option.id = response[i].id;
              selectList.appendChild(option);
            }

            container.appendChild(selectList);

            var id = $("#select").find('option:selected').attr('id');
            var url = "http://localhost/~ranaterning/htdocs/book-it/api/public/api/company-bookings-view/";

            get(id,url);
        });

    } else {
        var url = "http://localhost/~ranaterning/htdocs/book-it/api/public/api/company-bookings/";
        get(id,url);
    }

    function get(id,url) {

        var settings = {
            "async": true,
            "url": url+id,
            "method": "GET",
            "headers": {
            }
        }

        $.ajax(settings).done(function (response) {
            //Page title
            document.getElementById("page-title").innerHTML = response.company_name;
            //Company info
            var div = document.createElement("div");
            div.className = "booking-img";
            var url = `images/${response.image}`;
            div.style.backgroundImage = "url("+url+")";
            var bookingDiv = document.createElement("div");
            bookingDiv.className = "booking";
            var h2 = document.createElement("h2");
            var h2Text = document.createTextNode(response.company_name);
            h2.appendChild(h2Text);
            var textBox = document.createElement("p");
            var text = document.createTextNode(response.description);
            textBox.appendChild(text);
            bookingDiv.appendChild(h2);
            bookingDiv.appendChild(textBox);
            container.appendChild(div);
            container.appendChild(bookingDiv);

            var x;
            for (x in response.bookings) {
                //Company bookings
                var bookingsDiv = document.createElement("div");
                bookingsDiv.className = "bookings";
                bookingsDiv.id = response.bookings[x].id;
                var imgDiv = document.createElement("div");
                imgDiv.className = "small-img";
                var url = `images/${response.bookings[x].image}`;
                imgDiv.style.backgroundImage = "url("+url+")";
                bookingsDiv.appendChild(imgDiv);
                var textBox = document.createElement("div");
                textBox.className = "text-box";
                bookingsDiv.appendChild(textBox);
                var strong = document.createElement("strong");
                title = document.createTextNode(response.bookings[x].title);
                strong.appendChild(title);
                textBox.appendChild(strong);
                var p = document.createElement("p");
                price = document.createTextNode(response.bookings[x].price+" kr");
                p.appendChild(price);
                textBox.appendChild(p);
                container.appendChild(bookingsDiv);

                var id = response.bookings[x].id;

                if(trueOrFalse) {
                    bookingsDiv.onclick = (function(id){
                       return function(){
                           getBooking(id,true);
                       }
                   })(id);
               } else {
                   bookingsDiv.onclick = (function(id){
                      return function(){
                          getBooking(id,false);
                      }
                  })(id);
               }

            }
        });
    }

}
