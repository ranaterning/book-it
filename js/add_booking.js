function plusFunction(e){
    var input = document.getElementById('capacity');
    input.value = parseInt(input.value, 10) +1;
}

function minusFunction(e){
    var input = document.getElementById('capacity');

    if(input.value > 1){
        input.value = parseInt(input.value, 10) -1;
    }
}
//Add a booking
function addBookingForm(){
    container.innerHTML = "";

    document.getElementById("page-title").innerHTML = "LÄGG TILL BOKNINGSBART TILLFÄLLE";

    $(".user-icons").find("div").css("background-color", "#f7f6f6");
    $(".icon-calendar-empty").css("background-color", "#e6d5d5");

    //Getting companies belonging to a user
    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "http://localhost/~ranaterning/htdocs/book-it/api/public/api/user-companies/"+userId,
        "method": "GET",
        "headers": {
            "Authorization": "Bearer "+apiToken
        }
    }

    //Form for the booking-info
    $.ajax(settings).done(function (response) {
        var x;
        var selectArray = [];

        for(x in response){
            selectArray.push(response[x].company_name);
        }

        var selectLabel = document.createElement("Label");
        selectLabel.htmlFor = "select";
        selectLabel.innerHTML="Företag/Bokningsägare:";

        var selectList = document.createElement("select");
        selectList.id = "select";

        for (var i = 0; i < selectArray.length; i++) {
            var option = document.createElement("option");
            option.value = selectArray[i];
            option.text = selectArray[i];
            option.id = response[i].id;
            selectList.appendChild(option);
        }

        var bookingInfoDiv = document.createElement("div");
        bookingInfoDiv.className = "add-booking-info";
        bookingInfoDiv.id = "add-booking-info";
        container.appendChild(bookingInfoDiv);

        var f = document.createElement("form");
        f.setAttribute('method',"post");
        f.id = "addbooking_form";
        bookingInfoDiv.appendChild(f);

        var img = document.createElement("img");
        img.id = "img";
        img.src="images/placeholder.png";
        f.appendChild(img);

        var imageLabel = document.createElement("Label");
        imageLabel.htmlFor = "image";
        imageLabel.innerHTML="Bild:";

        var image = document.createElement("input");
        image.setAttribute('type',"file");
        image.setAttribute('name',"image");
        image.id = "image";
        image.accept = "image/*";
        f.appendChild(imageLabel);
        f.appendChild(image);

        $("#image").change(function(){
            var input = document.getElementById("image");
            var fReader = new FileReader();
            fReader.readAsDataURL(input.files[0]);
            imgSrc = input.files[0].name;

            fReader.onloadend = function(e){
                var img = document.getElementById("img");
                img.src = e.target.result;
            }
        });

        f.appendChild(selectLabel);
        f.appendChild(selectList);

        var titleLabel = document.createElement("Label");
        titleLabel.htmlFor = "titel";
        titleLabel.innerHTML="Titel:*";

        var title = document.createElement("input");
        title.setAttribute('type',"text");
        title.setAttribute('name',"title");
        title.id = "titel";
        f.appendChild(titleLabel);
        f.appendChild(title);

        var priceLabel = document.createElement("Label");
        priceLabel.htmlFor = "price";
        priceLabel.innerHTML="Pris:";

        var price = document.createElement("input");
        price.setAttribute('type',"text");
        price.setAttribute('name',"price");
        price.id = "price";
        f.appendChild(priceLabel);
        f.appendChild(price);

        var descriptionLabel = document.createElement("Label");
        descriptionLabel.htmlFor = "description";
        descriptionLabel.innerHTML="Beskrivning:";

        var description = document.createElement("input");
        description.setAttribute('type',"text");
        description.setAttribute('name',"description");
        description.id = "description";
        f.appendChild(descriptionLabel);
        f.appendChild(description);

        var btn = document.createElement("button");
        btn.id = "booking-button";
        btnText = document.createTextNode("NÄSTA");
        btn.appendChild(btnText);
        f.appendChild(btn);

        btn.addEventListener('click', function(e){
            e.preventDefault();

            var compId = $("#select").find('option:selected').attr('id');

            var data = JSON.stringify({
                "user_id": userId,
                "company_id": compId,
                "title": $("#titel").val(),
                "description": $("#description").val(),
                "image": imgSrc,
                "price": $("#price").val(),
            });

            var xhr = new XMLHttpRequest();

            xhr.addEventListener("readystatechange", function (e) {
                if (this.readyState === 4) {
                    addBookingTimes(e,this.responseText,compId);
                }
            });

            xhr.open("POST", "http://localhost/~ranaterning/htdocs/book-it/api/public/api/add-booking-info");
            xhr.setRequestHeader("Content-Type", "application/json");
            xhr.setRequestHeader("Authorization", "Bearer "+apiToken);

            xhr.send(data);

        });

    });

}
