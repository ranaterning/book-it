<!DOCTYPE html>
<html>
<head>
	<title>BOOKIT</title>
	<meta charset="utf-8">
	<meta http-equiv="Content-type" content="text/html;charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="https://fonts.googleapis.com/css?family=Alegreya+Sans:100|Open+Sans:300,600|Josefin+Sans:100,300,400,600" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" type="text/css" href="font/icons.css">
	<link rel="stylesheet" type="text/css" href="css/buttons.css">
	<script src="js/index.js"></script>
	<script src="js/get_company_bookings.js"></script>
	<script src="js/navigation.js"></script>
	<script src="js/register.js"></script>
	<script src="js/login.js"></script>
	<script src="js/reserve_booking.js"></script>
	<script src="js/user_page.js"></script>
	<script src="js/user_company_page.js"></script>
	<script src="js/add_company.js"></script>
	<script src="js/add_booking.js"></script>
	<script src="js/add_booking_times.js"></script>
	<script src="js/edit_user_info.js"></script>
	<script src="js/user_reservations.js"></script>
	<script src="js/get_company_staff.js"></script>
	<script src="js/get_personal_schedule.js"></script>
	<script src="js/get_booked_appointments.js"></script>

	<link href='fullcalendar.min.css' rel='stylesheet' />
	<link href='fullcalendar.print.min.css' rel='stylesheet' media='print' />
	<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>

</head>

	<body id="body">

		<div class="menu-bg"></div>

		<header>
			<div class="icon-box">
				<a class="icon-mail-alt" href=""></a>
				<a class="icon-facebook-official" href=""></a>
				<a class="icon-user" id="log-in" href=""></a>
			</div>
			<img id="logo" src="images/bookit-logga.png">
			<p class="menu"><img src="images/meny.png"> </p>
			<p class="close-menu"> &times; </p>
		</header>

		<nav id="nav" class="main-menu"></nav>

		<main>
			<h1 id="page-title"></h1>
			<div id="user-menu"></div>
			<div id="container"></div>
		</main>

		<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
		<link href='fullcalendar.min.css' rel='stylesheet' />
		<link href='fullcalendar.print.min.css' rel='stylesheet' media='print' />
		<script src='lib/moment.min.js'></script>
		<script src='lib/jquery.min.js'></script>
		<script src='fullcalendar.min.js'></script>
		<script src='locale-all.js'></script>
	</body>
</html>
