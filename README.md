
# BOOKIT

A booking site based on three different "user stories", the company that creates the reservation and can handle its staff, staff who can see their schedule and manage their personal data, as well as the customer who can register and book appointments.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development.

### Prerequisites

* Install PHP and Mysql
* Install Composer https://getcomposer.org/download/
* Install Laravel https://laravel.com/docs/5.6/installation

### Installing

* Clone the repository: https://gitlab.com/ranaterning/book-it.git
* Create a database
* Create a .env file in /api directory and configure with your db settings
* Run php artisan migrate in the terminal to migrate all the tables to your database
* To send requests to the api go through root/book-it/api/public/api/
* Make sure you change all the urls in the /js directory to match your root folder


## Built With

* Laravel Framework for creating the Api https://laravel.com/docs/5.5/
* Illuminate/Database https://packagist.org/packages/illuminate/database
* AJAX Libraries API https://trends.builtwith.com/cdn/AJAX-Libraries-API
* jQuery https://trends.builtwith.com/javascript/jQuery
* Fullcalendar https://fullcalendar.io/download

## Versioning

* Gitlab https://gitlab.com/ranaterning/book-it

## Authors

* Rana Terning http://ranaterning.se
